function numberSeparator(x: number | string) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberUnit(num: number | string | undefined, exponent?: number): string {
  if (num === "" || num === undefined) return "";
  const number = Number(num);
  if (number < 1000) return number.toString();
  const units = ["K", "M", "B", "T"];
  const unitIndex = Math.floor(Math.log10(number) / 3) - 1;
  const unit = units[unitIndex];
  const scaledNum = number / Math.pow(1000, unitIndex + 1);

  return scaledNum.toFixed(exponent) + unit;
}
export { numberSeparator, numberUnit };
