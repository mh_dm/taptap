import { TapStorage } from "@/types/localstorage";
import { isClientSide } from "./side";

const add = <T>(key: string, value: T) => {
  if (isClientSide()) {
    localStorage.setItem(key, JSON.stringify(value));
  }
};

const get = <T>(key: string): T | undefined => {
  if (isClientSide()) {
    const data = localStorage.getItem(key);
    if (!data) return;
    return JSON.parse(data);
  }
};

const remove = (key: string) => {
  if (isClientSide()) localStorage.removeItem(key);
};

const setStorageTaps = (tapStorage: TapStorage) => {
  add("taps", tapStorage);
};

const getStorageTaps = () => {
  return get<TapStorage>("taps");
};

export { setStorageTaps, getStorageTaps };
