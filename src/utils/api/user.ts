import { Daily, User, Wallet } from "@prisma/client";
import { dailyParser } from "./daily";
import { walletParser } from "./wallet";

type FullUser = User & { daily: Daily | null; wallet: Wallet | null };
const userParser = (user: FullUser): FullUser => {
  return {
    ...user,
    daily: user.daily ? dailyParser(user.daily) : null,
    wallet: user.wallet ? walletParser(user.wallet) : null,
  };
};

export { userParser };
