const friendParser = (user: any): InviteUser => {
  return {
    identity: user.identity,
    firstName: user.firstName || "",
    lastName: user.lastName || "",
    avatar: user.avatar || "",
    wallet: { balance: user.wallet?.balance || 0, profit: user.wallet?.profit || 0 },
  };
};

export { friendParser };
