import { dailies } from "@/enums";
import { Daily as PrismaDaily } from "@prisma/client";
import dayjs from "dayjs";

/**
 *  This Function get a level and map to real level of user can see
 * for example:
 *  level in db -> user level
 *  1 -> 1
 *  9 -> 2
 *  50 -> 7
 */
const levelCalculator = (dbLevel: number, lastUpdate?: Date | null) => {
  let level = dbLevel;
  if (
    lastUpdate &&
    dayjs().isAfter(dayjs(lastUpdate).add(1, "day").endOf("D"))
  ) {
    level = 0;
  }

  if (
    lastUpdate &&
    dayjs().isAfter(dayjs(lastUpdate).endOf("D")) &&
    dayjs().isBefore(dayjs(lastUpdate).add(1, "day").endOf("D")) &&
    dailies[((level - 1) % Object.keys(dailies).length) + 1]?.isMax
  ) {
    level = 0;
  }
  return ((level - 1) % Object.keys(dailies).length) + 1;
};

/**
 * Daily parser function, get dbDaily and return real Daily:
 */
const dailyParser = (daily: PrismaDaily): Daily => {
  return {
    ...daily,
    level: levelCalculator(daily.level, daily.updatedAt),
    activeAt: daily.updatedAt
      ? dayjs(daily.updatedAt).add(1, "day").startOf("day").toDate()
      : null,
  };
};

export { levelCalculator, dailyParser };
