import { CardLevelPro, CardLevelType, CardType } from "@/types/card";
import { Card } from "@prisma/client";

const cardParser = (
  card: Partial<Card & { cardLevel?: Partial<CardLevelPro> }>
): CardType => {
  return {
    ...cardLevelParser(card.cardLevel),
    id: card.id || 0,
    title: card.title || "",
    image: card.image || (null as any),
    description: card.description || "",
    upgradeTitle: card.upgradeTitle || card.title || "",
    upgradeDescription: card.upgradeDescription || card.description || "",
    createdAt: card.createdAt || new Date(),
    expiredAt: card.expiredAt || null,
  };
};

const cardLevelParser = (cardLevel?: Partial<CardLevelPro>): CardLevelType => {
  return {
    id: cardLevel?.id || 0,
    level: cardLevel?.level || 0,
    cardId: cardLevel?.cardId || 0,
    price: cardLevel?.nextLevel?.price || 0,
    bonus: cardLevel?.nextLevel?.bonus || 0,
    profit: cardLevel?.profit || 0, /// total profit of previous levels
    profitDelta: cardLevel?.nextLevel?.profit || 0,
    isMaxLevel: !cardLevel?.nextLevelId,
  };
};
export { cardParser, cardLevelParser };
