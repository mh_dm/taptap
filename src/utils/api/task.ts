import { UserTask } from "@/types/task";
import { UserTask as PrismaUserTask, Task } from "@prisma/client";

const userTaskParser = (task: Task, userTask: PrismaUserTask): UserTask => {
  return {
    ...task,
    ...userTask,
  };
};

export { userTaskParser };
