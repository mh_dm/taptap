import { walletLevels } from "@/enums";
import { Wallet as PrismaWallet } from "@prisma/client";
import dayjs from "dayjs";

const levelCalculator = (peak: number): number => {
  return (
    Object.values(walletLevels).findLast((w) => peak > w.fromValue)?.level ||
    Object.values(walletLevels)[Object.values(walletLevels).length - 1].level
  );
};
const walletParser = (wallet: PrismaWallet): Wallet => {
  return {
    ...wallet,
    level: levelCalculator(wallet.peakBalance),
    balance:
      wallet.balance +
      (Math.min(-dayjs(wallet.updatedAt).diff() / 1000, 3600 * 3) *
        wallet.profit) /
        3600,
  };
};

export { walletParser, levelCalculator };
