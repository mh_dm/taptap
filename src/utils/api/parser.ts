import { getTranslations } from "next-intl/server";
import { cookies } from "next/headers";
import { parseTelegramUser } from "../telegram";
import { logger } from "./logger";

const statusList = {
  100: { message: "Continue", level: "info" },
  101: { message: "SwitchingProtocols", level: "info" },
  102: { message: "Processing", level: "info" },
  103: { message: "EarlyHints", level: "info" },
  200: { message: "OK", level: "info" },
  201: { message: "Created", level: "info" },
  202: { message: "Accepted", level: "info" },
  203: { message: "NonAuthoritativeInformation", level: "info" },
  204: { message: "NoContent", level: "info" },
  205: { message: "ResetContent", level: "info" },
  206: { message: "PartialContent", level: "info" },
  207: { message: "MultiStatus", level: "info" },
  208: { message: "AlreadyReported", level: "info" },
  226: { message: "IMUsed", level: "info" },
  300: { message: "MultipleChoices", level: "info" },
  301: { message: "MovedPermanently", level: "info" },
  302: { message: "Found", level: "info" },
  303: { message: "SeeOther", level: "info" },
  304: { message: "NotModified", level: "info" },
  307: { message: "TemporaryRedirect", level: "info" },
  308: { message: "PermanentRedirect", level: "info" },
  400: { message: "BadRequest", level: "error" },
  401: { message: "Unauthorized", level: "error" },
  402: { message: "PaymentRequired", level: "error" },
  403: { message: "Forbidden", level: "error" },
  404: { message: "NotFound", level: "error" },
  405: { message: "MethodNotAllowed", level: "error" },
  406: { message: "NotAcceptable", level: "error" },
  407: { message: "ProxyAuthenticationRequired", level: "error" },
  408: { message: "RequestTimeout", level: "error" },
  409: { message: "Conflict", level: "error" },
  410: { message: "Gone", level: "error" },
  411: { message: "LengthRequired", level: "error" },
  412: { message: "PreconditionFailed", level: "error" },
  413: { message: "ContentTooLarge", level: "error" },
  414: { message: "URITooLong", level: "error" },
  415: { message: "UnsupportedMediaType", level: "error" },
  416: { message: "RangeNotSatisfiable", level: "error" },
  417: { message: "ExpectationFailed", level: "error" },
  418: { message: "ImATeapot", level: "error" },
  421: { message: "MisdirectedRequest", level: "error" },
  422: { message: "UnprocessableContent", level: "error" },
  423: { message: "Locked", level: "error" },
  424: { message: "FailedDependency", level: "error" },
  425: { message: "TooEarly", level: "error" },
  426: { message: "UpgradeRequired", level: "error" },
  428: { message: "PreconditionRequired", level: "error" },
  429: { message: "TooManyRequests", level: "error" },
  431: { message: "RequestHeaderFieldsTooLarge", level: "error" },
  451: { message: "UnavailableForLegalReasons", level: "error" },
  500: { message: "InternalServerError", level: "error" },
  501: { message: "NotImplemented", level: "error" },
  502: { message: "BadGateway", level: "error" },
  503: { message: "ServiceUnavailable", level: "error" },
  504: { message: "GatewayTimeout", level: "error" },
  505: { message: "HTTPVersionNotSupported", level: "error" },
  506: { message: "VariantAlsoNegotiates", level: "error" },
  507: { message: "InsufficientStorage", level: "error" },
  508: { message: "LoopDetected", level: "error" },
  510: { message: "NotExtended", level: "error" },
  511: { message: "NetworkAuthenticationRequired", level: "error" },
};

interface ParserResponseType {
  data: any;
  status: keyof typeof statusList;
  message: string;
}
const responseParser = async ({
  data,
  status: sts,
  message,
  request,
  body,
}: Partial<ParserResponseType> & { request: Request; body?: any | null }) => {
  const cookieStore = cookies();
  const initData = cookieStore.get("initData")?.value || "";
  const t = await getLocale(request);
  const telegramUser = parseTelegramUser(initData, process.env.TELEGRAM_TOKEN);
  const status = sts || 200;
  const msg = statusList[status].message || "";
  logger[statusList[status].level as keyof typeof logger](
    JSON.stringify({
      userId: telegramUser?.id || null,
      initData: initData || "",
      url: request.url,
      body: body || null,
      message: message || msg,
    })
  );
  return Response.json(
    {
      data: data || null,
      status,
      message: message || t("Server." + msg),
    },
    { status, statusText: msg }
  );
};

const getLocale = async (request?: Request) => {
  const cookieStore = cookies();
  let language: string | null = null;
  if (request) {
    const { search } = new URL(request.url);
    const params = new URLSearchParams(search);
    language = params.get("locale");
  }
  const locale = language || cookieStore.get("NEXT_LOCALE")?.value || "en";
  const t = await getTranslations({
    locale,
  });
  return t;
};

const getTelegramUser = (request?: Request) => {
  const cookieStore = cookies();
  let initData;
  if (request) {
    const url = request.url;
    const { search } = new URL(url);
    initData = search.slice(1);
  } else {
    initData = cookieStore.get("initData")?.value || "";
  }
  const telegramUser = parseTelegramUser(initData, process.env.TELEGRAM_TOKEN);
  return { telegramUser, initData };
};

export { getLocale, getTelegramUser, responseParser };
export type { ParserResponseType };
