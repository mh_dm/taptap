const { PrismaClient } = require("@prisma/client");
const dayjs = require("dayjs");
const prisma = new PrismaClient();

async function aggregateBalances() {
  try {
    const result = await prisma.wallet.aggregate({
      _sum: {
        balance: true,
      },
    });

    const balanceSum = result._sum.balance || 0;

    const userCount = await prisma.user.count();
    const completedDaily = await prisma.daily.count({
      where: {
        updatedAt: {
          gte: dayjs().startOf("day"),
          lte: dayjs(),
        },
      },
    });

    await prisma.monitor.create({
      data: {
        balanceSum,
        completedDaily,
        userCount,
      },
    });

    console.log(
      "Aggregated balance saved:",
      userCount,
      balanceSum,
      completedDaily
    );
  } catch (error) {
    console.error("Error aggregating balances:", error);
  } finally {
    await prisma.$disconnect();
  }
}

aggregateBalances();
