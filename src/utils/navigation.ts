import { locales } from "@/enums";
import { createSharedPathnamesNavigation } from "next-intl/navigation";

export const { Link, redirect, usePathname, useRouter } =
  createSharedPathnamesNavigation({
    locales: Object.keys(locales) /* ... */,
  });
