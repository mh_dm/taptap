const isClientSide = (): boolean => {
  return typeof document !== "undefined";
};
const isServerSide = (): boolean => {
  return typeof document === "undefined";
};

export { isClientSide, isServerSide };
