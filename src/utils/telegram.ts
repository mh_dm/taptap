import crypto from "crypto";

interface TelegramUser {
  id: number;
  telegramId: string;
  isBot?: boolean;
  firstName: string;
  lastName: string;
  username: string;
  languageCode: string;
  isPremium?: boolean;
  addedToAttachmentMenu?: boolean;
  allowsWriteToPm: boolean;
  photoUrl?: string;
  referral?: string;
}

enum TelegramUserKey {
  id = "telegramId",
  userId = "id",
  is_bot = "isBot",
  first_name = "firstName",
  last_name = "lastName",
  username = "username",
  language_code = "languageCode",
  is_premium = "isPremium",
  added_to_attachment_menu = "addedToAttachmentMenu",
  allows_write_to_pm = "allowsWriteToPm",
  photo_url = "photoUrl",
}

const telegramUserParser: Record<keyof TelegramUser, (value: any) => any> = {
  id: (value: any) => value,
  telegramId: (value: any) => value.toString(),
  isBot: (value: any) => value,
  firstName: (value: any) => value,
  lastName: (value: any) => value,
  username: (value: any) => value,
  languageCode: (value: any) => value,
  isPremium: (value: any) => value,
  addedToAttachmentMenu: (value: any) => value,
  allowsWriteToPm: (value: any) => value,
  photoUrl: (value: any) => value,
  referral: (value: any) => value,
};

function parseTelegramUser(
  initData: string,
  token: string
): TelegramUser | undefined {
  const params = new URLSearchParams(initData);
  const receivedHash = params.get("hash");
  params.delete("hash");
  const dataCheckString = [...(params.entries() as any)]
    .sort(([a], [b]) => a.localeCompare(b))
    .map(([key, value]) => `${key}=${value}`)
    .join("\n");
  const secretKey = crypto
    .createHmac("sha256", "WebAppData")
    .update(token)
    .digest();
  const computedHash = crypto
    .createHmac("sha256", secretKey)
    .update(dataCheckString)
    .digest("hex");

  if (computedHash === receivedHash) {
    return {
      ...Object.entries(JSON.parse(params.get("user") || "{}")).reduce(
        (pre, [key, value]) => {
          return {
            ...pre,
            [TelegramUserKey[key as keyof typeof TelegramUserKey]]:
              telegramUserParser[
                TelegramUserKey[key as keyof typeof TelegramUserKey]
              ](value),
          };
        },
        {} as TelegramUser
      ),
      referral: params.get("start_param") || "",
      id: parseInt(params.get("userId")?.toString() || ""),
    };
  }
  return undefined;
}

function addUserIdToInitial(
  initData: string,
  userId: number,
  token: string
): string {
  const params = new URLSearchParams(initData);
  params.set("userId", userId.toString());
  params.delete("hash");
  const dataCheckString = [...(params.entries() as any)]
    .sort(([a], [b]) => a.localeCompare(b))
    .map(([key, value]) => `${key}=${value}`)
    .join("\n");
  const secretKey = crypto
    .createHmac("sha256", "WebAppData")
    .update(token)
    .digest();
  const computedHash = crypto
    .createHmac("sha256", secretKey)
    .update(dataCheckString)
    .digest("hex");
  params.set("hash", computedHash);
  return params.toString();
}

function generateReferralCode(): string {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let result = "";
  const charactersLength = characters.length;

  for (let i = 0; i < 10; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

function generateReferralLink(referralCode: string): string {
  const link = new URL(process.env.NEXT_PUBLIC_BOT_LINK);
  link.searchParams.append("startapp", referralCode);
  return link.href;
}

export {
  parseTelegramUser,
  generateReferralCode,
  generateReferralLink,
  addUserIdToInitial,
};
