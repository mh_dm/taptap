"use client";
import { tapStorages } from "@/enums";
import { useWallet } from "@/hooks";
import { IconBolt, IconRocket } from "@tabler/icons-react";
import { useRouter } from "next/navigation";
import { Counter } from "../counter";
import { Button } from "../button";

const Charge = () => {
  const { wallet } = useWallet();
  const router = useRouter();
  return (
    <div className="flex items-center gap-3">
      <div className="flex-1 relative bg-slate-700 rounded-lg flex p-2">
        <div
          className="absolute h-full bg-violet-700 z-0 top-0 left-0 rounded-lg transition-all duration-1000"
          style={{
            width:
              (wallet.charge * 100) / tapStorages[wallet.storage].value + "%",
          }}
        ></div>
        <IconBolt className="z-10" />
        <Counter value={wallet.charge} className="px-2" />
        <div className="z-10">/</div>
        <Counter value={tapStorages[wallet.storage].value} />
      </div>

      <Button
        onClick={() => {
          router.push("/tap-level");
        }}
      >
        <IconRocket />
      </Button>
    </div>
  );
};

export { Charge };
