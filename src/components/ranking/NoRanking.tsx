"use client";
import { useFriends } from "@/hooks/useFriend";
import { IconRefreshDot } from "@tabler/icons-react";
import Image from "next/image";

const NoRanking = () => {
  const { remainingSec, reload } = useFriends();
  return (
    <div className="flex flex-col items-center justify-center w-full flex-1 h-full gap-4">
      <Image
        alt="no-friends"
        src="/no_friends.webp"
        width={500}
        height={500}
        className="w-3/5"
      />
      <div className="flex flex-col items-center">
        <span className="text-lg">No One Here</span>
        <span className="text-sm text-slate-300">
          Be first...
        </span>
      </div>
      
    </div>
  );
};

export { NoRanking };
