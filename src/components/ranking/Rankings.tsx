"use client";

import { useUser, useWallet } from "@/hooks";
import { FriendItem } from "../friends";
import { NoRanking } from "./NoRanking";

const Rankings = ({
  rankings,
  loading,
  level,
}: {
  rankings: InviteUser[];
  loading?: boolean;
  level: number;
}) => {
  const { wallet } = useWallet();
  const { user } = useUser();
  const localRanking: InviteUser[] = [
    ...rankings.filter((a) => a.identity !== user.identity),
    ...(wallet.level === level
      ? [
          {
            identity: user.identity,
            avatar: user.avatar || "",
            firstName: user.firstName || "",
            lastName: user.lastName || "",
            wallet: { balance: wallet.balance, profit: wallet.profit },
          },
        ]
      : []),
  ].sort((a, b) => b.wallet.balance - a.wallet.balance);
  return (
    <div className="flex flex-col p-2 space-y-2 h-full w-full">
      {loading ? (
        [...Array(8)]?.map((a, i) => <FriendItem key={i} loading />)
      ) : (
        <>
          {localRanking.length === 0 ? (
            <NoRanking />
          ) : (
            <ul className="w-full space-y-1">
              {localRanking?.map((friend, i) => (
                <div
                  key={friend.identity}
                  className="flex items-center gap-3 w-full"
                >
                  <div className="w-6 text-center">{i + 1}</div>
                  <FriendItem {...friend} />
                </div>
              ))}
              <li className="p-6" />
            </ul>
          )}
        </>
      )}
    </div>
  );
};

export { Rankings };
