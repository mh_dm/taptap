"use client";

import { walletLevels } from "@/enums";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import { useRouter } from "next/navigation";
import { Button } from "../button";

const RankingHero = (params: { level: number }) => {
  const Icon = walletLevels[params.level].icon;
  const router = useRouter();
  return (
    <div className="relative flex flex-col items-center p-5 w-full">
      <Icon size={100} strokeWidth={1.2} />
      <span className="w-full text-center">Level {params.level}</span>
      {Number(params.level) > 1 && (
        <Button
          className="absolute left-0 top-1/2"
          onClick={() => {
            router.push(`/ranking/${params.level - 1}`);
          }}
        >
          <IconChevronLeft />
        </Button>
      )}
      {Number(params.level) < Object.keys(walletLevels).length && (
        <Button
          className="absolute right-0 top-1/2"
          onClick={() => {
            router.push(`/ranking/${params.level + 1}`);
          }}
        >
          <IconChevronRight />
        </Button>
      )}
    </div>
  );
};

export { RankingHero };
