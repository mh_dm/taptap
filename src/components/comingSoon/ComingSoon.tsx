import { useTranslations } from "next-intl";
import Image from "next/image";

const ComingSoon = () => {
  const t = useTranslations();

  return (
    <div className="flex flex-col items-center gap-4 mt-20 justify-center h-full">
      <Image
        src={"/coming_soon.webp"}
        alt="coming_soon"
        width={360}
        height={360}
        className="w-full mx-auto max-w-52"
      />
      <span className="text-xl">{t("ComingSoon")}</span>
    </div>
  );
};

export { ComingSoon };
