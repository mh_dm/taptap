import { PropsWithChildren } from "react";

const MissionWrapper = (params: PropsWithChildren) => {
  return (
    <div className="absolute top-[140px] ltr:right-0 rtl:left-0">
      <div className="px-4 space-y-3">{params.children}</div>
    </div>
  );
};

export { MissionWrapper };
