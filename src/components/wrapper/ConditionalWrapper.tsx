import React, { forwardRef } from "react";

// eslint-disable-next-line react/display-name
const ConditionalWrapper = forwardRef(
  (
    {
      condition,
      wrapper,
      children,
    }: {
      condition: boolean;
      wrapper: (children: React.ReactNode) => React.ReactNode;
      children: React.ReactNode;
    },
    ref: React.Ref<any>
  ) => {
    const childWithRef = React.isValidElement(children)
      ? React.cloneElement(children as React.ReactElement<any>, { ref })
      : children;

    return condition ? wrapper(childWithRef) : childWithRef;
  }
);

export { ConditionalWrapper };
