import { PropsWithChildren } from "react";

const BottomSectionWrapper = (params: PropsWithChildren) => {
  return (
    <div className="fixed bottom-20 left-1/2 -translate-x-1/2 w-full max-w-lg">
      <div className="px-4">{params.children}</div>
    </div>
  );
};

export { BottomSectionWrapper };
