"use client";
import { useWallet } from "@/hooks";
import { IconHandFinger, IconHourglassHigh } from "@tabler/icons-react";
import { DetailItem } from "./DetailItem";

const Details = () => {
  const { wallet } = useWallet();
  return (
    <ul className="flex gap-4">
      <DetailItem value={wallet.tap} icon={IconHandFinger} name="per tap" />
      <DetailItem value={wallet.profit} icon={IconHourglassHigh} name="profit" />
    </ul>
  );
};

export { Details };
