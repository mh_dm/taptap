"use client";
import { useUser } from "@/hooks/useUser";
import { Avatar } from "../avatar/Avatar";

const UserProfile = () => {
  const { user } = useUser();
  return (
    <div className="flex items-center gap-2">
      <Avatar src={user.avatar || "/avatar_5.jpg"} />
      <p className="text-sm overflow-hidden text-ellipsis max-w-full whitespace-nowrap">
        {user?.firstName} {user?.lastName}
      </p>
    </div>
  );
};

export { UserProfile };
