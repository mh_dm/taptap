interface DetailItemProps {
  name: string;
  value: number;
  icon: TablerIcon;
}

const DetailItem = (props: DetailItemProps) => {
  return (
    <li className="flex items-center gap-1">
      <props.icon size={20} />
      <p>{props.value}</p>
    </li>
  );
};

export { DetailItem };
