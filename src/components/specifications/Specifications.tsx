import { Details } from "./Details";
import { UserProfile } from "./UserProfile";

const Specifications = () => {
  return (
    <div className="w-full justify-between flex items-center bg-slate-800 px-4 py-2 rounded-lg mt-2">
      <UserProfile />
      <div className="flex">
        <div className="w-0.5 bg-slate-700 mx-2" />
        <Details />
      </div>
    </div>
  );
};

export { Specifications };
