"use client";
import { navbarItems } from "@/enums";
import { useTranslations } from "next-intl";
import { usePathname } from "next/navigation";
import { useMemo } from "react";
import { NavbarItem } from "./NavbarItem";

const Navbar = ({ className }: { className?: string }) => {
  const pathname = usePathname();
  const t = useTranslations();
  const selectedIndex = useMemo(
    () =>
      navbarItems.findIndex(
        (item) =>
          item.url === "/" + (pathname.split("?")[0].split("/")[2] || "")
      ),
    [pathname]
  );
  return (
    <div className={`fixed bottom-0 w-full max-w-lg  px-4 ${className}`}>
      <div
        className={`shadow-xl w-full max-w-lg bg-slate-800 rounded-t-xl ${className}`}
      >
        <div
          className="bg-slate-900 w-16 aspect-square rounded-full opacity-20 absolute transition-all duration-500"
          style={{ left: (selectedIndex + 0.2) * 25 + "%" }}
        ></div>
        <ul className="relative grid grid-cols-4 justify-around py-3">
          {navbarItems.map((item, i) => (
            <NavbarItem
              key={item.name}
              {...item}
              name={t(item.name)}
              selected={selectedIndex === i}
            />
          ))}
        </ul>
      </div>
    </div>
  );
};

export { Navbar };
