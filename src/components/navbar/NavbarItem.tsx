import { NavbarItemType } from "@/enums";
import Image from "next/image";
import { ConditionalWrapper } from "../wrapper/ConditionalWrapper";
import { Link } from "@/utils/navigation";

type NavbarItemProps = NavbarItemType & {
  selected?: boolean;
  badge?: boolean;
};
const NavbarItem = (props: NavbarItemProps) => {
  return (
    <li>
      <ConditionalWrapper
        condition={!props.disabled}
        wrapper={(children) => <Link href={props.url}>{children}</Link>}
      >
        <div
          className={`w-full flex flex-col items-center gap-1 transition-all duration-300 ${
            props.disabled ? "opacity-30" : props.selected ? "" : "opacity-50"
          }`}
        >
          <div className="relative">
            {"image" in props ? (
              <Image
                src={props.image}
                alt={props.name}
                width={64}
                height={64}
                className="aspect-square w-8"
              />
            ) : (
              <props.icon />
            )}
            {props.badge ? (
              <span className="absolute top-0 -right-1 rounded-full bg-red-600 w-1.5 aspect-square"></span>
            ) : null}
          </div>
          <span className="capitalize text-xs">{props.name}</span>
        </div>
      </ConditionalWrapper>
    </li>
  );
};

export { NavbarItem };
