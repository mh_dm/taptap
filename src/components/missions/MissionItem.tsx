"use client"
interface MissionItemProps {
  icon: TablerIcon;
  name: string;
  onClick?: () => void;
}
const MissionItem = (props: MissionItemProps) => {
  return (
    <div
      className="flex flex-col items-center justify-center bg-slate-800 p-2 aspect-square rounded-lg w-12"
      onClick={() => props.onClick?.()}
    >
      <props.icon size={20} strokeWidth={1.5} />
      <span className="text-xs opacity-80 max-w-full overflow-hidden">{props.name}</span>
    </div>
  );
};

export { MissionItem };
