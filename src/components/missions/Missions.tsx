"use client";
import { IconCalendar, IconLanguage, IconUpload } from "@tabler/icons-react";
import { MissionWrapper } from "../wrapper";
import { MissionItem } from "./MissionItem";
import { useState } from "react";
import DailyRewardModal from "../modal/daily/DailyRewardsModal";
import { useTranslations } from "next-intl";
import LanguageModal from "../modal/LanguageModal";
import { useRouter } from "@/utils/navigation";

const Missions = () => {
  const [openDaily, setOpenDaily] = useState(false);
  const [openLang, setOpenLang] = useState(false);
  const router = useRouter();
  const t = useTranslations();
  return (
    <MissionWrapper>
      <MissionItem
        icon={IconCalendar}
        name={t("Daily")}
        onClick={() => setOpenDaily(true)}
      />
      <MissionItem
        icon={IconLanguage}
        name={t("Lang")}
        onClick={() => setOpenLang(true)}
      />
      <MissionItem
        icon={IconUpload}
        name={t("Upgrade")}
        onClick={() => router.push("/upgrade")}
      />
      <DailyRewardModal
        isOpen={openDaily}
        onClose={() => {
          setOpenDaily(false);
        }}
      />
      <LanguageModal
        isOpen={openLang}
        onClose={() => {
          setOpenLang(false);
        }}
      />
    </MissionWrapper>
  );
};

export { Missions };
