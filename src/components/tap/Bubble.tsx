import { perTaps } from "@/enums";
import { useUser, useWallet } from "@/hooks";

interface BubbleType {
  id: number;
  x: number;
  y: number;
}
const Bubble = (bubble: BubbleType) => {
  const { wallet } = useWallet();
  return (
    <div
      key={bubble.id}
      className="absolute  pointer-events-none animate-bubble"
      style={{ left: bubble.x, top: bubble.y }}
    >
      <span className="text-4xl">+{perTaps[wallet.tap].value}</span>
    </div>
  );
};

export { Bubble };
export type { BubbleType };
