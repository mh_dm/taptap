"use client";
import { useWallet } from "@/hooks";
import { IconBolt } from "@tabler/icons-react";
import { useRef, useState } from "react";
import { Bubble, BubbleType } from "./Bubble";
import { perTaps } from "@/enums";

interface TapProps {}

const Tap = ({}: TapProps) => {
  const { onTap, wallet } = useWallet();
  const bubbleId = useRef(0);
  const [bubbles, setBubbles] = useState<BubbleType[]>([]);
  const [rotate, setRotate] = useState({ x: 0, y: 0 });
  const isActive = wallet.charge >= perTaps[wallet.tap].value;

  const onTapHandler = (
    clientX: number,
    clientY: number,
    containerRect: DOMRect
  ) => {
    const newBubble = {
      id: bubbleId.current++,
      x: clientX - containerRect.left,
      y: clientY - containerRect.top,
    };

    setBubbles((prevBubbles) => [...prevBubbles, newBubble]);
    const halfWidth = containerRect.width / 2;
    const halfHeight = containerRect.height / 2;

    setRotate({
      x: ((newBubble.y - halfHeight) / halfHeight) * -10,
      y: ((newBubble.x - halfWidth) / halfWidth) * 10,
    });
    setTimeout(() => {
      setBubbles((prevBubbles) =>
        prevBubbles.filter((bubble) => bubble.id !== newBubble.id)
      );
    }, 2000);
    setTimeout(() => {
      setRotate({ x: 0, y: 0 });
    }, 100);
    onTap();
  };

  const onClickHandler = (e: React.MouseEvent<HTMLDivElement>) => {
    if (process.env.NEXT_PUBLIC_NODE === "development") {
      const containerRect = e.currentTarget.getBoundingClientRect();
      onTapHandler(e.clientX, e.clientY, containerRect);
    }
  };

  const onTouchStartHandler = (e: React.TouchEvent<HTMLDivElement>) => {
    const containerRect = e.currentTarget.getBoundingClientRect();
    Array.from(e.touches).forEach((touch) =>
      onTapHandler(touch.clientX, touch.clientY, containerRect)
    );
  };

  return (
    <div className="relative w-3/5">
      <div
        style={{
          transform: `perspective(600px) rotateX(${rotate.x}deg) rotateY(${rotate.y}deg)`,
        }}
        onClick={isActive ? onClickHandler : undefined}
        onTouchStart={isActive ? onTouchStartHandler : undefined}
        className={`relative w-full ${
          isActive ? "bg-violet-700" : "bg-slate-600"
        } transition-all duration-100 rounded-full aspect-square flex items-center justify-center`}
      >
        <div className="absolute shadow-inner-2 w-4/5 aspect-square rounded-full"></div>
        <IconBolt
          size={120}
          strokeWidth={1.2}
          style={{
            filter: "drop-shadow( 0px 4px 2px rgba(0, 0, 0, .05))",
          }}
        />
      </div>
      {bubbles.map((bubble) => (
        <Bubble key={bubble.id} {...bubble} />
      ))}
    </div>
  );
};

export { Tap };
