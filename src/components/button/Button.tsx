interface ButtonProps {
  variant?: "bg" | "text" | "ring";
}

const Button = ({
  children,
  ...props
}: ButtonProps &
  React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >) => {
  return (
    <button
      {...props}
      className={`flex-0 flex items-center gap-1 ${
        props.variant === "ring"
          ? "ring-1 text-purple-500 ring-purple-500 disabled:text-slate-700 disabled:ring-slate-700"
          : props.variant === "text"
          ? "text-purple-500 disabled:text-slate-700"
          : "bg-purple-500 text-black disabled:bg-slate-700"
      } rounded-sm py-3 px-6 uppercase font-medium ${props.className} justify-center`}
    >
      {children}
    </button>
  );
};

export { Button };
