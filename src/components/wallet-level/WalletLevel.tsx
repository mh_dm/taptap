"use client";
import { walletLevels } from "@/enums";
import { useWallet } from "@/hooks";
import { numberUnit } from "@/utils";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";

const WalletLevel = () => {
  const { wallet } = useWallet();
  const router = useRouter();
  const t = useTranslations();
  return (
    <div className="flex flex-col gap-1 w-full">
      <div className="flex items-center justify-between">
        <span
          className="text-xs"
          onClick={() => {
            router.push(`/ranking/${wallet.level}`);
          }}
        >
          {t("Ranking")}
        </span>
        <span className="text-xs">
          {t("NextLevel")}{" "}
          {numberUnit(walletLevels[wallet.level + 1]?.fromValue)}
        </span>
      </div>
      <div className="flex-1 relative bg-slate-700 rounded-lg flex p-1">
        <div
          className="absolute h-full bg-violet-700 z-0 top-0 left-0 rounded-lg transition-all duration-1000"
          style={{
            width:
              Math.min(
                (wallet.balance * 100) /
                  (walletLevels[wallet.level + 1]?.fromValue || 1),
                100
              ) + "%",
          }}
        ></div>
      </div>
    </div>
  );
};

export { WalletLevel };
