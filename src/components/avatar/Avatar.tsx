import Image, { ImageProps } from "next/image";

const Avatar = ({
  loading,
  ...props
}: Omit<ImageProps, "alt" | "width" | "height" | "loading"> &
  Partial<{
    alt: string;
    width: number;
    height: number;
    loading?: boolean;
  }>) => {
  if (loading) {
    return (
      <div
        className={`min-w-max bg-slate-500 bg-opacity-50 animate-pulse rounded-full aspect-square w-8 ${
          props.className ?? ""
        }`}
      />
    );
  }
  return (
    <Image
      {...props}
      alt={props.alt ?? "avatar"}
      width={props.width ?? 64}
      height={props.width ?? 64}
      className={`rounded-full aspect-square w-8 ${props.className ?? ""}`}
    />
  );
};

export { Avatar };
