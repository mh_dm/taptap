import { Button } from "../button";

const Controller = ({
  count,
  currentIndex,
  onNext,
  onPrevious,
  onSkip,
}: {
  count: number;
  currentIndex: number;
  onNext: () => void;
  onPrevious: () => void;
  onSkip: () => void;
}) => {
  return (
    <div className="absolute bottom-2 flex flex-col items-center w-full">
      <div className="w-full flex items-center gap-2 px-3">
        {[...Array(count)].map((a, i) => (
          <div key={i} className={`h-0.5 w-full ${currentIndex >= i ? "bg-slate-300" : "bg-slate-600"} `} />
        ))}
      </div>
      <div className="flex items-center gap-2 w-full p-3">
        {currentIndex > 0 && <Button onClick={onPrevious}>previous</Button>}
        <Button className="w-full" onClick={onSkip}>
          Continue
        </Button>
        {currentIndex < count - 1 && <Button onClick={onNext}>Next</Button>}
      </div>
    </div>
  );
};

export { Controller };
