"use client";
import Image from "next/image";
import { Introduction as PrismaIntroduction } from "@prisma/client";
import { useState } from "react";
import { Introduction } from "./Introduction";
import { Controller } from "./Controller";

const controller = 1;
const Introductions = ({
  introductions,
}: {
  introductions: PrismaIntroduction[];
}) => {
  const [currentSlide, setCurrentSlide] = useState(1);
  return (
    <div className="flex flex-col h-full">
      <Introduction introduction={introductions[currentSlide - 1]} />
      <Controller
        count={3}
        currentIndex={currentSlide - 1}
        onNext={() => {setCurrentSlide(currentSlide + 1)}}
        onSkip={() => {}}
        onPrevious={() => {setCurrentSlide(currentSlide - 1)}}
      />
    </div>
  );
};

export { Introductions };
