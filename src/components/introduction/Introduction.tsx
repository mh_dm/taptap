import { Introduction as PrismaIntroduction } from "@prisma/client";
import Image from "next/image";

const Introduction = ({
  introduction,
}: {
  introduction: PrismaIntroduction;
}) => {
  return (
    <div className="flex flex-col h-dvh">
      <Image
        src={introduction.image}
        alt={introduction.title}
        width={360}
        height={360}
        className="w-full max-h-[500px] flex-0 object-cover object-center"
      />
      <div className="flex-1 h-full p-6">
        <h1 className="text-4xl uppercase mb-5">{introduction.title}</h1>
        <p className="">{introduction.description}</p>
      </div>
    </div>
  );
};

export { Introduction };
