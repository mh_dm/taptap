import { IconBolt } from "@tabler/icons-react";
import Modal, { ModalProps } from "./Modal";
import { useWallet } from "@/hooks";
import { Button } from "../button";

interface UpgradeModalProps extends ModalProps {
  price?: number;
  currentValue?: number;
  nextValue?: number;
  description?: string;
  icon?: TablerIcon;
  title?: string;
  onUpgrade?: () => void;
}

const UpgradeModal = (props: UpgradeModalProps) => {
  const { wallet } = useWallet();
  const disabled = wallet.balance < (props.price || 0);
  return (
    <Modal
      isOpen={props.isOpen || false}
      onClose={() => {
        props.onClose?.();
      }}
      className="items-center gap-3"
    >
      {props.icon ? <props.icon size={72} strokeWidth={1.5} /> : null}
      <h1 className="text-lg">Upgrade {props.title}</h1>
      {props.currentValue ||
        (props.nextValue && (
          <p className="text-sm opacity-80">
            {props.currentValue} {"->"} {props.nextValue} (+
            {(props.nextValue || 0) - (props.currentValue || 0)})
          </p>
        ))}
      {!!props.description && (
        <p className="text-sm opacity-80">{props.description}</p>
      )}
      <div className="flex items-center gap-2">
        <IconBolt />
        <p className="text-2xl">{props.price}</p>
      </div>
      <Button
        disabled={disabled}
        onClick={() => {
          !disabled && props.onUpgrade?.();
        }}
      >
        {disabled ? "Not Enough Bolt" : "Upgrade Now"}
      </Button>
    </Modal>
  );
};

export default UpgradeModal;
