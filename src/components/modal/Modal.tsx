import { PropsWithChildren, useEffect, useState } from "react";
import { createPortal } from "react-dom";

interface ModalProps extends PropsWithChildren {
  isOpen: boolean;
  onClose: () => void;
  className?: string;
}
const Modal = ({ isOpen, onClose, children, className }: ModalProps) => {
  const [isVisible, setIsVisible] = useState(isOpen);

  useEffect(() => {
    const handleScroll = (e: any) => e.preventDefault();

    if (isOpen) {
      setIsVisible(true);
      document.body.addEventListener("scroll", handleScroll, {
        passive: false,
      });
    } else {
      const timer = setTimeout(() => {
        setIsVisible(false);
        document.body.removeEventListener("scroll", handleScroll);
      }, 195); // Duration of slide-down animation
      return () => clearTimeout(timer);
    }

    return () => {
      document.body.removeEventListener("scroll", handleScroll);
    };
  }, [isOpen]);

  if (!isVisible) return null;

  return createPortal(
    <div
      className="fixed inset-0 bg-black bg-opacity-50 flex items-end justify-center max-h-dvh overflow-hidden"
      onClick={onClose}
    >
      <div
        className={`bg-slate-800 w-full max-w-md rounded-t-xl p-4 shadow-lg ${
          isOpen ? "animate-slide-up" : "animate-slide-down"
        }`}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={`my-2 flex flex-col w-full ${className || ""}`}>
          {children}
        </div>
      </div>
    </div>,
    document.body
  );
};

export default Modal;

export type { ModalProps };
