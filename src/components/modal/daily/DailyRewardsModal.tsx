import { dailies } from "@/enums";
import { useDaily } from "@/hooks/useDaily";
import Modal, { ModalProps } from "../Modal";
import { DailyItem } from "./DailyItem";
import { useTranslations } from "next-intl";
import { Button } from "@/components/button";

interface DailyRewardModalProps extends ModalProps {
  level?: number;
}

const DailyRewardModal = (props: DailyRewardModalProps) => {
  const { daily: dailyHook, activeLevel, onClime } = useDaily();
  const t = useTranslations();
  return (
    <Modal
      isOpen={props.isOpen || false}
      onClose={() => {
        props.onClose?.();
      }}
      className="items-center gap-3"
    >
      <div className="flex flex-col w-full gap-3 mb-8">
        <h1 className="text-lg">{t("DailyRewards")}</h1>
        <div className="grid grid-cols-4 w-full gap-3">
          {Object.values(dailies).map((daily) => (
            <DailyItem
              key={daily.level}
              level={daily.level}
              claimed={daily.level <= dailyHook.level}
              active={activeLevel === daily.level}
            />
          ))}
        </div>
      </div>
      <Button
        disabled={activeLevel === 0}
        onClick={() => {
          props.onClose();
          onClime();
        }}
      >
        {activeLevel === 0 ? "Go back Tomorrow" : "Clime Now"}
      </Button>
    </Modal>
  );
};

export default DailyRewardModal;
