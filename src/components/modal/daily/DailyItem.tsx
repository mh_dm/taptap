import { dailies } from "@/enums";
import { numberUnit } from "@/utils";
import { IconBolt } from "@tabler/icons-react";

const DailyItem = (props: {
  level: number;
  className?: string;
  claimed?: boolean;
  active?: boolean;
  onClick?: (level: number) => void;
}) => {
  const Icon = dailies[props.level].icon;
  return (
    <div
      className={`w-full rounded-lg ${
        props.claimed ? "bg-teal-600 shadow-teal-400" : "bg-slate-700"
      }  ${props.className || ""} ${
        dailies[props.level].isMax ? "h-full col-span-2" : "aspect-square"
      }`}
      onClick={() => {
        props.active && props.onClick?.(props.level);
      }}
    >
      <div
        className={`w-full flex flex-col items-center justify-center h-full p-1 ${
          props.active ? "text-amber-500" : ""
        }`}
      >
        <span className="text-xs opacity-80">Day</span>
        <Icon size={28} strokeWidth={1} className="flex-1" />
        <div className="flex-0 flex items-center gap-0.5">
          <IconBolt size={18} />
          <span className="text-sm">{numberUnit(dailies[props.level].value)}</span>
        </div>
      </div>
    </div>
  );
};

export { DailyItem };
