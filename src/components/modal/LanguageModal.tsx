"use client";
import { locales } from "@/enums";
import { IconCheck } from "@tabler/icons-react";
import { useLocale, useTranslations } from "next-intl";
import Modal, { ModalProps } from "./Modal";
import { useRouter } from "@/utils/navigation";

interface DailyRewardModalProps extends ModalProps {}

const LanguageModal = (props: DailyRewardModalProps) => {
  const t = useTranslations();
  const currentLocale = useLocale();
  const router = useRouter();
  return (
    <Modal
      isOpen={props.isOpen || false}
      onClose={() => {
        props.onClose?.();
      }}
      className="items-center gap-3"
    >
      <div className="flex flex-col w-full gap-3 mb-8">
        <h1 className="text-lg">{t("Lang")}</h1>
        {Object.values(locales).map((locale) => (
          <div
            className={`py-2 px-4 flex ${
              currentLocale === locale.code ? "bg-slate-700" : ""
            } rounded-lg`}
            key={locale.code}
            onClick={() => {
              router.replace("/", { locale: locale.code });
            }}
          >
            <span className="flex-1">
              {locale.code.toLocaleUpperCase()} - {t(locale.name)}
            </span>
            {currentLocale === locale.code && <IconCheck className="flex-0" />}
          </div>
        ))}
      </div>
    </Modal>
  );
};

export default LanguageModal;
