"use client";
import { IconBolt } from "@tabler/icons-react";
import { Counter } from "./Counter";
import { useWallet } from "@/hooks";

const CoinCounter = () => {
  const { wallet } = useWallet();
  return (
    <div className="flex gap-1 items-center transition-all duration-1000">
      <IconBolt size={38} />
      <Counter value={wallet.balance} className="[&>*]:text-4xl" />
    </div>
  );
};
export { CoinCounter };
