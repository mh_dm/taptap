"use client"
import { numberSeparator } from "@/utils";
import { motion } from "framer-motion";
import { useEffect, useRef, useState } from "react";
type DeltaType = "increase" | "decrease" | "";

const usePrevious = (value: number) => {
  const ref = useRef(0);
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
};

const formatForDisplay = (number: number, exponent?: number) => {
  return numberSeparator(number.toFixed(exponent ?? 0))
    .split("")
    .reverse();
};

const NumberColumn = ({
  digit,
  delta,
}: {
  digit: number;
  delta: DeltaType;
}) => {
  const [position, setPosition] = useState(0);
  const columnContainer = useRef<HTMLDivElement>(null);

  const setColumnToNumber = (number: number) => {
    setPosition((columnContainer.current?.clientHeight || 0) * number);
  };

  useEffect(() => setColumnToNumber(digit), [digit]);

  return (
    <div
      ref={columnContainer}
      style={{
        height: "auto",
      }}
    >
      <motion.div
        animate={{ x: 0, y: position }}
        className={`absolute h-[1000%] bottom-0`}
      >
        {[9, 8, 7, 6, 5, 4, 3, 2, 1, 0].map((num) => (
          <div key={num} className="flex flex-col-reverse items-center h-[10%]">
            <span>{num}</span>
          </div>
        ))}
      </motion.div>
      <span className="invisible">0</span>
    </div>
  );
};

const Counter = ({
  value = 0,
  exponent = 0,
  className = "",
}: {
  value: number;
  exponent?: number;
  className?: string;
}) => {
  const numArray = formatForDisplay(value, exponent);
  const previousNumber = usePrevious(value);
  const delta: DeltaType = value > previousNumber ? "increase" : "decrease";
  return (
    <motion.div
      layout
      className={`flex flex-row-reverse overflow-hidden relative h-auto ${className}`}
      dir="ltr"
    >
      {numArray.map((number, index) =>
        number === "." || number === "," ? (
          <div key={index}>
            <span>{number}</span>
          </div>
        ) : (
          <NumberColumn key={index} digit={Number(number)} delta={delta} />
        )
      )}
    </motion.div>
  );
};

export { Counter };
