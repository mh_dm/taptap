import { AbstractIntlMessages, NextIntlClientProvider } from "next-intl";
import { PropsWithChildren } from "react";

interface IntlProviderProps extends PropsWithChildren {
  local: string;
  messages: AbstractIntlMessages;
}

const IntlProvider = (props: IntlProviderProps) => {
  return (
    <NextIntlClientProvider locale={props.local} messages={props.messages}>
      {props.children}
    </NextIntlClientProvider>
  );
};

export { IntlProvider };

export type { IntlProviderProps };
