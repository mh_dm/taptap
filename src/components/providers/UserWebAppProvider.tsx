"use client";
import Loading from "@/app/[locale]/(app)/loading";
import { useUser } from "@/hooks";
import Script from "next/script";
import {
  PropsWithChildren,
  createContext,
  useEffect,
  useRef,
  useState,
} from "react";

interface UserWebAppContext {
  webApp: any;
}
const defaultUserWebAppContext: UserWebAppContext = {
  webApp: undefined,
};
const WebAppContext = createContext<UserWebAppContext>(
  defaultUserWebAppContext
);
interface UserProviderProps {}
const WebAppProvider = ({ children }: PropsWithChildren<UserProviderProps>) => {
  const [contextState, setContextState] = useState<UserWebAppContext>(
    defaultUserWebAppContext
  );
  const { loading } = useUser();
  const intervalRef = useRef<NodeJS.Timeout | undefined>();
  useEffect(() => {
    intervalRef.current = setInterval(() => {
      const initData =
        process.env.NEXT_PUBLIC_NODE === "production"
          ? (window as any)?.Telegram?.WebApp?.initData
          : "user=%7B%22id%22%3A107546604%2C%22first_name%22%3A%22%F0%9D%99%BC%F0%9D%99%BE%F0%9D%99%B7%F0%9D%99%B0%F0%9D%99%BC%F0%9D%99%BC%F0%9D%99%B0%F0%9D%99%B3%20%F0%9D%99%B7%22%2C%22last_name%22%3A%22%22%2C%22username%22%3A%22MH_DM%22%2C%22language_code%22%3A%22en%22%2C%22allows_write_to_pm%22%3Atrue%7D&chat_instance=-8454637327437198090&chat_type=group&auth_date=1718645773&hash=b39ab25b40113600fef62c480f4e8f3a23b05472ff9d680f7d5e4137276c3133";
      if (initData) {
        clearInterval(intervalRef.current);
        setContextState({ webApp: (window as any)?.Telegram?.WebApp });
      }
    }, 500);
    return () => {
      intervalRef.current && clearInterval(intervalRef.current);
    };
  }, []);
  return (
    <WebAppContext.Provider value={contextState}>
      <div className="bg-slate-700"></div>
      <Script src="https://telegram.org/js/telegram-web-app.js" defer></Script>

      {!loading ? children : <Loading />}
    </WebAppContext.Provider>
  );
};

export { WebAppContext, WebAppProvider };

export type { UserProviderProps };
