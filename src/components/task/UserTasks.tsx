"use client";
import { useWallet } from "@/hooks";
import api from "@/services";
import { UserTask } from "@/types/task";
import { userTaskParser } from "@/utils/api/task";
import { Task as PrismaTask, UserTask as PrismaUserTask } from "@prisma/client";
import { useCallback, useEffect, useState } from "react";
import UpgradeModal from "../modal/UpgradeModal";
import { Task } from "./Task";

const UserTasks = ({ tasks }: { tasks: PrismaTask[] }) => {
  const [openUpgrade, setOpenUpgrade] = useState<
    UserTask & { open: boolean }
  >();

  const [genTasks, setGenTasks] = useState<UserTask[]>(
    tasks.map((task) => ({ ...task, completedAt: null, userId: 0, taskId: 0 }))
  );
  const { setWallet } = useWallet();
  const setUserTaskToCards = useCallback(
    (userTasks: PrismaUserTask[]) => {
      const uts = userTasks.reduce((pre, cur) => {
        const task = tasks.find((task) => task.id === cur.taskId);
        if (!task) {
          return [...pre];
        }
        return [...pre, { ...userTaskParser(task, cur), id: task.id }];
      }, [] as UserTask[]);

      setGenTasks((genTasks) => {
        uts.forEach((uc) => {
          genTasks.splice(
            genTasks.findIndex((gc) => gc.id === uc.taskId),
            1,
            uc
          );
        });
        return [...genTasks];
      });
    },
    [tasks]
  );
  useEffect(() => {
    api.get("/api/user/task").then((res) => {
      setUserTaskToCards(res.data);
    });
  }, [setUserTaskToCards]);
  return (
    <ul className="w-full space-y-1 mt-3 mb-16">
      {genTasks.map((task, i) => (
        <Task
          key={task.id}
          task={task}
          onClick={(task) => {
            setOpenUpgrade({ ...task, open: true });
          }}
        />
      ))}
      <UpgradeModal
        isOpen={!!openUpgrade?.open}
        onClose={() => setOpenUpgrade((ou: any) => ({ ...ou, open: false }))}
        title={openUpgrade?.title || ""}
        description={"Bonus +" + openUpgrade?.bonus}
        onUpgrade={() => {
          api
            .post("/api/user/task", { taskId: openUpgrade?.id })
            .then((res) => {
              console.log(res.data);
              setOpenUpgrade((ou: any) => ({ ...ou, open: false }));
              setUserTaskToCards([res.data.task]);
              res.data.wallet && setWallet(res.data.wallet);
            });
        }}
      />
    </ul>
  );
};

export { UserTasks };
