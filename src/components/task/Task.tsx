"use client";
import { UserTask } from "@/types/task";
import { IconCheck } from "@tabler/icons-react";
import Image from "next/image";

interface CardProps {
  task: UserTask;
  onClick: (card: UserTask) => void;
}
const Task = ({ task, onClick }: CardProps) => {
  return (
    <li
      className="bg-slate-800 rounded-lg p-2 flex items-center gap-3"
      onClick={() => {
        onClick?.(task);
      }}
    >
      <Image
        src={task.image}
        alt={task.title}
        width={48}
        height={48}
        className="rounded-lg flex-0 mx-auto aspect-square object-cover"
      />
      <div className="flex-1 -space-y-0.5">
        <p className="text-lg">{task.title}</p>
        <p className="text-xs opacity-65">{task.bonus}</p>
      </div>
      {!!task.completedAt && <IconCheck className="flex-0" />}
    </li>
  );
};

export { Task };
