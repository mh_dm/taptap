"use client";
import { CardType } from "@/types/card";
import { IconBolt } from "@tabler/icons-react";
import Image from "next/image";

interface CardProps {
  card: CardType;
  onClick: (card: CardType) => void;
}

const Card = ({ card, onClick }: CardProps) => {
  return (
    <div
      className="bg-slate-800 rounded-lg p-2 flex flex-col items-center gap-1"
      onClick={() => {
        onClick?.(card);
      }}
    >
      <Image
        src={card.image}
        alt={card.title}
        width={360}
        height={360}
        className="mx-auto w-3/5 aspect-square object-cover"
      />
      <p className="text-lg">{card.title}</p>
      <p className="text-xs opacity-65">{card.description}</p>
      <p className="text-xs opacity-65">profit: {card.profit}</p>
      <div className="border-t border-slate-700 w-full"></div>
      <div className="flex w-full">
        <div className="flex-0 flex gap-1 items-center justify-center px-2">
          <span className="">level {card.level}</span>
        </div>
        <div className="border-l border-slate-700 h-full"></div>
        <div className="flex-1 flex gap-1 items-center justify-center w-full px-2">
          {!card.isMaxLevel ? (
            <>
              <IconBolt size={18} />
              <span>{card.price || 0}</span>
            </>
          ) : (
            <span>MaxLevel</span>
          )}
        </div>
      </div>
    </div>
  );
};

export { Card };
