"use client";
import { useWallet } from "@/hooks";
import api from "@/services";
import { CardLevelMinimal, CardLevelPro, CardType } from "@/types/card";
import { cardParser } from "@/utils/api/card";
import { CardLevel, Card as PrismaCard } from "@prisma/client";
import { useCallback, useEffect, useState } from "react";
import UpgradeModal from "../modal/UpgradeModal";
import { Card } from "./Card";

const UpgradeCards = ({
  cards,
}: {
  cards: (PrismaCard & {
    levels: (CardLevelPro & { previousLevel: CardLevel })[];
  })[];
}) => {
  const [openUpgrade, setOpenUpgrade] = useState<
    CardType & { open: boolean }
  >();
  const [genCards, setGenCards] = useState<CardType[]>(
    cards.map((card) => {
      const { previousLevel, ...firstLevel } =
        card.levels.find((a) => !a.previousLevel) || {};
      return cardParser({
        ...card,
        cardLevel: {
          nextLevelId: "id" in firstLevel ? firstLevel.id : undefined,
          nextLevel: "id" in firstLevel ? firstLevel : undefined,
        },
      });
    })
  );
  const { setWallet } = useWallet();
  const setUserLevelsToCards = useCallback(
    (userLevels: CardLevelMinimal[]) => {
      const userCards = userLevels.reduce((pre, cur) => {
        const { levels, ...card } =
          cards.find((c) => c.id === cur.cardId) || {};
        const sortedLevels = levels?.sort((a, b) => a.level - b.level);
        const { previousLevel, ...finedLevel } = sortedLevels?.find(
          (l) => l.id === cur.id
        ) as CardLevelPro & { previousLevel: CardLevel };

        return [
          ...pre,
          cardParser({
            ...card,
            cardLevel: {
              ...finedLevel,
              profit: sortedLevels
                ?.slice(
                  0,
                  sortedLevels.findIndex((a) => a.id === finedLevel?.id) + 1
                )
                .reduce((pre, cur) => pre + cur.profit, 0),
            },
          }),
        ];
      }, [] as CardType[]);

      setGenCards((genCards) => {
        userCards.forEach((uc) => {
          genCards.splice(
            genCards.findIndex((gc) => gc.id === uc.id),
            1,
            uc
          );
        });
        return genCards;
      });
    },
    [cards]
  );
  useEffect(() => {
    api.get("/api/user/card/levels").then((res) => {
      setUserLevelsToCards(res.data);
    });
  }, [setUserLevelsToCards]);
  return (
    <div className="grid grid-cols-2 w-full gap-2 mt-3 mb-16">
      {genCards.map((card, i) => (
        <Card
          key={card.id}
          card={card}
          onClick={() => {
            if (!card.isMaxLevel)
              setOpenUpgrade((ou: any) => ({ ...card, open: true }));
          }}
        />
      ))}
      <UpgradeModal
        isOpen={!!openUpgrade?.open}
        onClose={() => setOpenUpgrade((ou: any) => ({ ...ou, open: false }))}
        price={openUpgrade?.price}
        title={openUpgrade?.upgradeTitle || ""}
        description={"+" + openUpgrade?.profitDelta + " profit per hour"}
        onUpgrade={() => {
          api
            .post("/api/user/card/upgrade", { cardId: openUpgrade?.id })
            .then((res) => {
              setUserLevelsToCards([res.data.cardLevel]);
              setOpenUpgrade((ou: any) => ({ ...ou, open: false }));
              setWallet(res.data.wallet);
            });
        }}
      />
    </div>
  );
};

export { UpgradeCards };
