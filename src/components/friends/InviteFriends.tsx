"use client";
import { useUser } from "@/hooks";
import { generateReferralLink } from "@/utils";
import { IconCopy } from "@tabler/icons-react";
import { useTranslations } from "next-intl";
import Link from "next/link";
import toast from "react-hot-toast";
import { Button } from "../button";
const InviteFriends = () => {
  const { user } = useUser();
  const t = useTranslations();
  const link = generateReferralLink(user?.identity || "");
  const text = "plz click on this link ...";
  return (
    <div className="w-full flex gap-3 justify-between">
      <Link
        href={`https://t.me/share/url?url=${link}&text=${text}`}
        className="flex-1 bg-violet-700 rounded-lg p-2 items-center flex justify-center font-medium h-full"
      >
        {t("InviteAFriend")}
      </Link>
      <Button
        onClick={() => {
          toast.success(t("YourLinkCopySuccess"));
          navigator.clipboard.writeText(`
            ${text}
            ${link}
            `);
        }}
      >
        <IconCopy />
      </Button>
    </div>
  );
};

export { InviteFriends };
