import { Avatar } from "../avatar";
import { FriendDetails } from "./FriendDetails";

const FriendItem = (
  friend:
    | (InviteUser & { loading?: boolean })
    | (Partial<InviteUser> & { loading: boolean })
) => {
  return (
    <li className="flex items-center gap-2 bg-slate-800 px-4 rounded-lg py-2 justify-between w-full">
      <div className="flex items-center gap-2">
        <Avatar
          src={friend.avatar || "/avatar_5.jpg"}
          loading={friend.loading}
        />
        <p
          className={`text-sm overflow-hidden text-ellipsis max-w-full whitespace-nowrap ${
            friend.loading
              ? "rounded-md bg-slate-500 bg-opacity-50 w-32 h-4 animate-pulse"
              : ""
          }`}
          style={{
            width: 100 + Math.random() * 50 + "px",
          }}
        >
          {friend.firstName} {friend.lastName}
        </p>
      </div>
      <div className="flex">
        <div className="w-0.5 bg-slate-700 mx-2" />
        <FriendDetails {...friend} />
      </div>
    </li>
  );
};

export { FriendItem };
