"use client";
import { IconBolt, IconHourglassHigh } from "@tabler/icons-react";
import { FriendDetailItem } from "./FriendDetailItem";

const FriendDetails = (
  friend:
    | (InviteUser & { loading?: boolean })
    | (Partial<InviteUser> & { loading: boolean })
) => {
  return (
    <ul className="flex gap-3">
      <FriendDetailItem
        value={friend.wallet?.balance}
        icon={IconBolt}
        name="coin"
        loading={friend.loading}
      /> 
      <FriendDetailItem
        value={friend.wallet?.profit}
        icon={IconHourglassHigh}
        name="profit"
        loading={friend.loading}
      />
    </ul>
  );
};

export { FriendDetails };
