import { numberUnit } from "@/utils";

type DetailItemProps = {
  name?: string;
  value?: number;
  loading?: boolean;
  icon: TablerIcon;
};

const FriendDetailItem = (props: DetailItemProps) => {
  return (
    <li className="flex items-center gap-1 min-w-16 max-w-16">
      <props.icon
        size={16}
        className={`min-h-max ${props.loading ? "animate-pulse opacity-50" : ""}`}
      />
      <p className={`text-sm ${props.loading ? "rounded-md bg-slate-500 bg-opacity-50 w-8 h-4 animate-pulse" : ""}`}>
        {numberUnit(props.value || 0)}
      </p>
    </li>
  );
};

export { FriendDetailItem };
