"use client";

import { useFriends } from "@/hooks/useFriend";
import { FriendItem } from "./FriendItem";
import { useEffect, useRef } from "react";
import { NoFriends } from "./NoFriends";
import { IconRefreshDot } from "@tabler/icons-react";
import { useTranslations } from "next-intl";
import { Button } from "../button";

const Friends = () => {
  const t = useTranslations();
  const { loading, friends, reload, lastCall, remainingSec } = useFriends();
  const intervalRef = useRef<NodeJS.Timeout | null>();
  useEffect(() => {
    intervalRef.current = setInterval(() => {}, 1000);
    return () => {
      intervalRef.current && clearInterval(intervalRef.current);
    };
  }, [lastCall]);
  return (
    <div className="flex flex-col space-y-2 h-full">
      {friends.length > 0 && (
        <>
          <div className="fixed top-0 left-1/2 -translate-x-1/2 w-full max-w-lg px-2">
            <div className="bg-slate-800 flex justify-between items-center py-2 px-4 rounded-b-lg shadow-lg">
              <h1 className="text-lg">{t("Friends")}</h1>
              <Button variant="text" disabled={remainingSec > 0} onClick={reload}>
                <IconRefreshDot size={16} /> {t("CheckAgain")}
              </Button>
            </div>
          </div>
          <div className="p-4"></div>
        </>
      )}
      <div className="flex-1 p-2">
        {loading ? (
          [...Array(8)]?.map((a, i) => <FriendItem key={i} loading />)
        ) : (
          <>
            {friends.length === 0 ? (
              <NoFriends />
            ) : (
              <ul className="w-full space-y-1">
                {friends?.map((friend, i) => (
                  <FriendItem key={friend.identity} {...friend} />
                ))}
                <li className="p-16"></li>
              </ul>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export { Friends };
