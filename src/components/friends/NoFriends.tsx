"use client";
import { useFriends } from "@/hooks/useFriend";
import { IconRefreshDot } from "@tabler/icons-react";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { Button } from "../button";

const NoFriends = () => {
  const { remainingSec, reload } = useFriends();
  const t = useTranslations();
  return (
    <div className="flex flex-col items-center justify-center w-full flex-1 h-full gap-4">
      <Image
        alt="no-friends"
        src="/no_friends.webp"
        width={500}
        height={500}
        className="w-3/5"
      />
      <div className="flex flex-col items-center">
        <span className="text-lg">{t("YouHaveNoFriends")}</span>
        <span className="text-sm text-slate-300">
          {t("InviteYourFriendsWithLinks")}
        </span>
      </div>
      <Button
        disabled={remainingSec > 0}
        className="disabled:text-slate-600 flex items-center gap-2 text-violet-500"
        onClick={reload}
      >
        <IconRefreshDot size={16} /> {t("CheckAgain")}
      </Button>
    </div>
  );
};

export { NoFriends };
