// app/page.tsx
"use client";

import { Button } from "@/components";
import api from "@/services";
import { useState } from "react";

export default function Home() {
  const [file, setFile] = useState<File | null>(null);
  const [url, setUrl] = useState("");
  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setFile(e.target.files[0]);
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (!file) return;

    const formData = new FormData();
    formData.append("file", file);

    api.upload("/api/upload", formData).then((res) => {
      console.log(res.data);
      setUrl(res.data.url);
    });
  };

  return (
    <div>
      <h1>Upload Image</h1>
      <form onSubmit={handleSubmit} className="flex-col flex gap-3">
        <input type="file" onChange={handleFileChange} />
        <Button type="submit">Upload</Button>
      </form>
      <p>{url}</p>
    </div>
  );
}
