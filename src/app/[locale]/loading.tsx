import { useTranslations } from "next-intl";

export default function Loading() {
  const t = useTranslations();
  return (
    <div className="flex w-full justify-center items-center h-dvh">
      {t("Loading")} ....
    </div>
  );
}
