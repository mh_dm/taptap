import { useTranslations } from "next-intl";
import Image from "next/image";

export default function Loading() {
  const t = useTranslations();
  return (
    <div className="relative flex w-full justify-center items-center h-dvh">
      <Image
        src={"/images/bck.jpg"}
        width={433}
        height={772}
        alt="loading"
        className="absolute h-full w-full overflow-hidden object-cover blur-[1px]"
      />
      <div className="bg-black opacity-20 absolute w-full h-full" />
      <Image
        src={"/icons/logo.svg"}
        width={150}
        height={50}
        alt="loading"
        className="absolute top-1/3 w-1/3 min-w-[150px]"
      />
      <span className="absolute bottom-1/4 font-medium text-xl uppercase">
        {t("Loading")} ....
      </span>
    </div>
  );
}
