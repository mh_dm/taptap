import { Introductions } from "@/components/introduction/Introductions";
import api from "@/services";

export default async function Home() {
  const { data } = await api.get(
    process.env.URL + "/api/introductions",
    {},
    { cache: "no-cache" }
  );
  return <Introductions introductions={data} />;
}
