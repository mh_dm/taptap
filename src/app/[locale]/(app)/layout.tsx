import { Navbar, WebAppProvider } from "@/components";

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <main className="relative flex flex-col min-h-dvh w-full max-w-lg bg-slate-900">
      <WebAppProvider>{children}</WebAppProvider>
    </main>
  );
}
