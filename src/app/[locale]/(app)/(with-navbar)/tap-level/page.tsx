"use client";
import { CoinCounter } from "@/components";
import UpgradeModal from "@/components/modal/UpgradeModal";
import { perTaps, tapStorages } from "@/enums";
import { useWallet } from "@/hooks";
import { IconBattery, IconBolt, IconHandFinger } from "@tabler/icons-react";
import { useState } from "react";

const UpgradeItem = (props: {
  price?: number;
  currentValue: number;
  nextValue?: number;
  icon: TablerIcon;
  title: string;
  onClick?: () => void;
}) => {
  return (
    <div
      className="bg-slate-800 rounded-lg flex gap-2 items-center w-full py-2 px-4 cursor-pointer"
      onClick={() => {
        props.price ? props.onClick?.() : undefined;
      }}
    >
      <props.icon size={36} className="flex-0" strokeWidth={1.3} />
      <div className="flex-1">
        <p>{props.title}</p>
        {props.nextValue ? (
          <p className="text-sm opacity-80">
            {props.currentValue} {"->"} {props.nextValue} (+
            {props.nextValue - props.currentValue})
          </p>
        ) : (
          <p className="text-sm opacity-80">{props.currentValue} </p>
        )}
      </div>
      {props.price ? (
        <div className="flex-0 flex items-center gap-1">
          <IconBolt />
          <p>{props.price}</p>
        </div>
      ) : (
        <p className="text-sm opacity-80">Max Level</p>
      )}
    </div>
  );
};
export default function Home() {
  const [upgradeModal, setUpgradeModal] = useState<{
    price?: number;
    currentValue?: number;
    nextValue?: number;
    icon?: TablerIcon;
    title?: string;
    open?: boolean;
  }>({});
  const { wallet, upgradeTap, upgradeStorage } = useWallet();
  return (
    <>
      <div className="flex-0 w-full flex flex-col items-center gap-2 mt-4">
        <div className="my-8">
          <CoinCounter />
        </div>
        <UpgradeItem
          title={"Per Tap"}
          icon={IconHandFinger}
          currentValue={perTaps[wallet.tap].value}
          nextValue={perTaps[wallet.tap + 1]?.value}
          price={perTaps[wallet.tap + 1]?.price}
          onClick={() => {
            setUpgradeModal({
              title: "Per Tap",
              icon: IconHandFinger,
              currentValue: perTaps[wallet.tap].value,
              nextValue: perTaps[wallet.tap + 1]?.value,
              price: perTaps[wallet.tap + 1]?.price,
              open: true,
            });
          }}
        />
        <UpgradeItem
          title={"Storage"}
          icon={IconBattery}
          currentValue={tapStorages[wallet.storage].value}
          nextValue={tapStorages[wallet.storage + 1]?.value}
          price={tapStorages[wallet.storage + 1]?.price}
          onClick={() => {
            setUpgradeModal({
              title: "Storage",
              icon: IconBattery,
              currentValue: tapStorages[wallet.storage].value,
              nextValue: tapStorages[wallet.storage + 1]?.value,
              price: tapStorages[wallet.storage + 1]?.price,
              open: true,
            });
          }}
        />
      </div>
      <UpgradeModal
        title={upgradeModal.title}
        isOpen={upgradeModal.open || false}
        icon={upgradeModal.icon}
        currentValue={upgradeModal.currentValue}
        nextValue={upgradeModal.nextValue}
        price={upgradeModal.price}
        onClose={() => {
          setUpgradeModal((u) => ({
            ...u,
            open: false,
          }));
        }}
        onUpgrade={() => {
          setUpgradeModal((u) => ({
            ...u,
            open: false,
          }));
          if (upgradeModal.title?.includes("Storage")) {
            upgradeStorage();
          } else {
            upgradeTap();
          }
        }}
      />
    </>
  );
}
