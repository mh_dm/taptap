import { BottomSectionWrapper, Friends, InviteFriends } from "@/components";

export default function Home() {
  return (
    <div className="relative w-full h-full">
      <Friends />
      <BottomSectionWrapper>
        <InviteFriends />
      </BottomSectionWrapper>
    </div>
  );
}
