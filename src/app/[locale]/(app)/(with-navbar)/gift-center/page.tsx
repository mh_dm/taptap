import { ComingSoon } from "@/components";

export default function Home() {
  return (
    <>
      <ComingSoon />
    </>
  );
}
