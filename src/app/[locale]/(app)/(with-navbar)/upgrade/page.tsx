import { UpgradeCards } from "@/components/card";
import api from "@/services";

export default async function Home() {
  const cards = await api.get(
    process.env.URL + "/api/cards",
    {},
    {
      next: { revalidate: 60 * 15 },
    }
  );
  return (
    <>
      <UpgradeCards cards={cards.data} />
    </>
  );
}
