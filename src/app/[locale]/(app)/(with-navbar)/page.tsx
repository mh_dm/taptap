import {
  BottomSectionWrapper,
  Charge,
  CoinCounter,
  Missions,
  Specifications,
  Tap,
} from "@/components";
import { WalletLevel } from "@/components/wallet-level/WalletLevel";

export default function Home() {
  return (
    <>
      <div className="flex-0 w-full flex flex-col items-center gap-8">
        <Specifications />
        <WalletLevel />
        <CoinCounter />
      </div>
      <div className="flex-1 flex items-center justify-center w-full">
        <Tap />
      </div>
      <Missions />
      <BottomSectionWrapper>
        <Charge />
      </BottomSectionWrapper>
    </>
  );
}
