import { RankingHero, Rankings } from "@/components/ranking";
import api from "@/services";

export default async function Page({ params }: { params: { level: number } }) {
  const ranking = await api.get(
    process.env.URL + "/api/ranking",
    {
      level: params.level,
    },
    {
      next: { revalidate: 60 * 5 },
    }
  );

  return (
    <div className="w-full items-center flex flex-col">
      <RankingHero level={Number(params.level)} />
      <div className="bg-slate-800 h-0.5 w-full"></div>
      <Rankings rankings={ranking.data as any} level={Number(params.level)} />
    </div>
  );
}
