import { UserTasks } from "@/components/task";
import api from "@/services";

export default async function Home() {
  const tasks = await api.get(
    process.env.URL + "/api/tasks",
    {},
    {
      cache: "no-cache",
      // next: { revalidate: 60 * 15 },
    }
  );
  return (
    <>
      <UserTasks tasks={tasks.data} />
    </>
  );
}