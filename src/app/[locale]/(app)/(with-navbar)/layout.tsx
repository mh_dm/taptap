import { Navbar } from "@/components";

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <div className="flex-1 flex flex-col items-center gap-5 w-full max-w-lg overflow-y-auto no-scroll px-4">
        {children}
        <div className="h-16" />
      </div>
      <Navbar className="flex-0" />
    </>
  );
}
