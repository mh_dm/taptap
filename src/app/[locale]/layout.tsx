import { IntlProvider } from "@/components";
import { locales } from "@/enums";
import type { Metadata, Viewport } from "next";
import { getMessages } from "next-intl/server";
import localFont from "next/font/local";
import { Toaster } from "react-hot-toast";
import "../globals.css";

export const metadata: Metadata = {
  title: "Nova Game",
  description: "Nova Space Station",
};

const localFonts = localFont({
  src: [
    {
      path: "../../../public/fonts/ChakraPetch-Light.ttf",
      weight: "200",
    },
    {
      path: "../../../public/fonts/ChakraPetch-Regular.ttf",
      weight: "300",
    },
    {
      path: "../../../public/fonts/ChakraPetch-Medium.ttf",
      weight: "400",
    },
    {
      path: "../../../public/fonts/ChakraPetch-SemiBold.ttf",
      weight: "500",
    },
    {
      path: "../../../public/fonts/ChakraPetch-Bold.ttf",
      weight: "700",
    },
  ],
  variable: "--local-fonts",
});
export const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};
export default async function RootLayout({
  children,
  params: { locale },
}: Readonly<{
  children: React.ReactNode;
  params: { locale: Locale };
}>) {
  const messages = await getMessages();
  return (
    <html lang={locale} dir={locales[locale].dir}>
      <body
        className={`${localFonts.className} max-w-full overflow-hidden bg-neutral-900 flex justify-center max-h-dvh`}
      >
        <IntlProvider local={locale} messages={messages}>
          {children}
        </IntlProvider>
        <Toaster
          position="top-center"
          reverseOrder={false}
          gutter={8}
          containerClassName=""
          containerStyle={{}}
          toastOptions={{
            duration: 3000,
            style: {
              background: "#334155",
              opacity: 80,
              color: "#fff",
            },
          }}
        />
      </body>
    </html>
  );
}
