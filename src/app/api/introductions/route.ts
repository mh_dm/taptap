import { responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  try {
    const introductions = await prisma.introduction.findMany();
    return responseParser({
      data: introductions,
      request,
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
