import { responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  try {
    const cards = await prisma.card.findMany({
      include: {
        levels: { include: { nextLevel: true, previousLevel: true } },
      },
    });
    return responseParser({
      data: cards,
      request,
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
