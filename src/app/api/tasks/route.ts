
import { responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  try {
    const tasks = await prisma.task.findMany({});
    return responseParser({
      data: tasks,
      request,
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
