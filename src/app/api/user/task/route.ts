import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { walletParser } from "@/utils/api/wallet";
import prisma from "@/utils/db";
import { TaskType } from "@prisma/client";
import dayjs from "dayjs";

const taskFunctions: Record<TaskType, () => boolean> = {
  InviteFriends: () => true,
  JoinTelegram: () => true,
  JoinX: () => true,
};
export async function GET(request: Request) {
  const t = await getLocale(request);
  const body: any = request.body;
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  try {
    const UserTasks = await prisma.userTask.findMany({
      where: {
        userId: telegramUser.id,
      },
    });

    return responseParser({
      data: UserTasks,
      request,
    });
  } catch (e) {}
  return responseParser({ status: 500, request });
}
export async function POST(request: Request) {
  const url = request.url;
  const body = await request.json();
  const t = await getLocale(request);
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  if (!body || !("taskId" in body)) {
    return responseParser({
      status: 422,
      request,
      body,
      message: t("Server.XIsRequired", {
        x: "'taskId'",
      }),
    });
  }
  try {
    let userTask = await prisma.userTask.findUnique({
      where: {
        userTaskIdentifier: {
          taskId: Number(body.taskId),
          userId: telegramUser.id,
        },
      },
      include: { task: true },
    });

    if (!userTask) {
      userTask = await prisma.userTask.create({
        data: {
          taskId: Number(body.taskId),
          userId: telegramUser.id,
        },
        include: { task: true },
      });
      if (userTask.task.waitingTime) {
        return responseParser({
          data: {
            task: userTask,
            wallet: null,
          },
          status: 200,
          request,
        });
      }
    }
    if (userTask.completedAt) {
      return responseParser({
        status: 406,
        request,
        body,
        message: t("Server.XImpossibleToDoCause", {
          x: "Check task",
          case: "Completed",
        }),
      });
    }

    if (dayjs().diff(userTask.createdAt) < userTask.task.waitingTime * 1000) {
      return responseParser({
        status: 406,
        request,
        body,
        message: t("Server.WaitingFor", {
          for: (
            (userTask.task.waitingTime * 1000 -
              dayjs().diff(userTask.createdAt)) /
            1000
          ).toFixed(),
        }),
      });
    }
    const preWallet = await prisma.wallet.findUnique({
      where: { userId: telegramUser.id },
    });
    if (!preWallet) {
      return responseParser({
        status: 404,
        request,
        message: t("Server.XNotFound", { x: t("Wallet") }),
      });
    }
    const newBalance = walletParser(preWallet).balance + userTask.task.bonus;

    const [newWallet, updatedTask] = await prisma.$transaction([
      prisma.wallet.update({
        where: { userId: telegramUser.id },
        data: {
          balance: newBalance,
          updatedAt: new Date(),
        },
      }),
      prisma.userTask.update({
        where: {
          userTaskIdentifier: {
            taskId: Number(body.taskId),
            userId: telegramUser.id,
          },
        },
        data: {
          completedAt: new Date(),
        },
      }),
    ]);
    return responseParser({
      data: {
        task: updatedTask,
        wallet: walletParser(newWallet),
      },
      status: 200,
      request,
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
