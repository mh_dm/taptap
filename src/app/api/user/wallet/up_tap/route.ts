import { perTaps } from "@/enums";
import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { walletParser } from "@/utils/api/wallet";
import prisma from "@/utils/db";

export async function POST(request: Request) {
  const t = await getLocale(request);
  const body = await request.json();
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });

  try {
    const wallet = await prisma.wallet.findUnique({
      where: { id: telegramUser.id },
    });

    if (!wallet) {
      return responseParser({
        status: 404,
        request,
        body,
        message: t("Server.XNotFound", { x: "Wallet" }),
      });
    } else if (!perTaps[wallet.tap]) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XCantUpgradeCause", {
          x: t("PerTap"),
          cause: t("MaxLevel"),
        }),
      });
    }
    const currentBalance = wallet.balance;

    if (currentBalance < perTaps[wallet.tap + 1].price) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XNotEnough", {
          x: t("Balance"),
        }),
      });
    }
    const newBalance =
      walletParser(wallet).balance - perTaps[wallet.tap + 1].price;
    const updatedWallet = await prisma.wallet.update({
      where: { id: telegramUser.id },
      data: {
        balance: newBalance,
        tap: wallet.tap + 1,
        updatedAt: new Date(),
      },
    });
    return responseParser({
      data: walletParser(updatedWallet),
      request,
      body,
      message: t("Server.XUpgradedTo", {
        x: t("PerTap"),
        to: updatedWallet.tap,
      }),
    });
  } catch (e) {
    return responseParser({ status: 500, request, body });
  }
}
