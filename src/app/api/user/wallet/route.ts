import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";

export async function POST(request: Request) {
  const t = await getLocale(request);
  const url = request.url;
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  const body = await request.json();

  // const wallet = await prisma.wallet.update({
  //   where: { id: telegramUser.id },
  //   data: {
  //     ...body,
  //     updatedAt: new Date(),
  //   },
  // });
  // if (wallet) return responseParser({ data: wallet, url });
  return responseParser({ status: 500, request });
}
