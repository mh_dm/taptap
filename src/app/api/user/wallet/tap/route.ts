import { perTaps, tapStorages } from "@/enums";
import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { walletParser } from "@/utils/api/wallet";
import prisma from "@/utils/db";

export async function POST(request: Request) {
  const body = await request.json();
  const t = await getLocale(request);
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  const wallet = await prisma.wallet.findUnique({
    where: { id: telegramUser.id },
  });
  try {
    if (!wallet) {
      return responseParser({
        status: 404,
        request,
        body,
        message: t("Server.XNotFound", { x: t("Wallet") }),
      });
    } else if (!("taps" in body)) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XIsRequired", {
          x: "'taps'",
        }),
      });
    } else if (!("startTime" in body)) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XIsRequired", {
          x: "'startTime'",
        }),
      });
    } else if (!("endTime" in body)) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XIsRequired", {
          x: "'endTime'",
        }),
      });
    } else if (body.endTime - body.startTime < (body.taps - 1) * 10) {
      return responseParser({
        status: 451,
        data: wallet,
        request,
        body,
        message: t("Server.XNotTrustedCause", {
          x: t("Taps"),
          cause: t("TimeLimit"),
        }),
      });
    } else if (body.endTime - body.startTime > body.taps * 5000) {
      return responseParser({
        status: 451,
        request,
        body,
        message: t("Server.XNotTrustedCause", {
          x: t("Taps"),
          cause: t("OverTime"),
        }),
      });
    }
    const startCharge = Math.min(
      (wallet?.charge || 0) +
        parseInt(
          ((new Date(body.startTime || new Date()).getTime() -
            new Date(wallet?.updatedAt || new Date()).getTime()) *
            3) /
            1000 +
            ""
        ),
      tapStorages[wallet?.storage || 1].value
    );

    const atEndCharge =
      startCharge +
      parseInt(
        ((new Date(body.endTime || new Date()).getTime() -
          new Date(body.startTime || new Date()).getTime()) *
          3) /
          1000 +
          ""
      );
    if (body.taps * perTaps[wallet.tap].value > atEndCharge + 3) {
      return responseParser({
        status: 451,
        message: t("Server.XNotTrustedCause", {
          x: t("Taps"),
          cause: t("OverCharge"),
        }),
        body,
        request,
      });
    }
    const newBalance =
      walletParser(wallet).balance + body.taps * perTaps[wallet.tap].value;
    const updatedWallet = await prisma.wallet.update({
      where: { id: telegramUser.id },
      data: {
        charge: atEndCharge - body.taps * perTaps[wallet.tap].value,
        balance: newBalance,
        peakBalance: Math.max(wallet.peakBalance, newBalance),
        updatedAt: new Date(),
      },
    });
    return responseParser({
      data: walletParser(updatedWallet),
      request,
      body,
    });
  } catch (e) {
    return responseParser({ status: 500, request, body });
  }
}
