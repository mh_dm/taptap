import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { walletParser } from "@/utils/api/wallet";
import prisma from "@/utils/db";

export async function POST(request: Request) {
  const url = request.url;
  const body = await request.json();
  const t = await getLocale(request);
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  if (!body || !("cardId" in body)) {
    return responseParser({
      status: 422,
      request,
      body,
      message: t("Server.XIsRequired", {
        x: "'cardId'",
      }),
    });
  }
  try {
    const card = await prisma.card.findUnique({
      where: { id: Number(body.cardId) },
      include: { levels: { include: { previousLevel: true } } },
    });

    if (!card) {
      return responseParser({
        status: 404,
        request,
        body,
        message: t("Server.XNotFound", { x: t("Card") }),
      });
    }
    let userCard = await prisma.userCard.findFirst({
      where: {
        userId: telegramUser.id,
        cardLevel: { cardId: Number(body.cardId) },
      },
      include: { cardLevel: true },
    });
    let nextCardLevel;
    if (!userCard) {
      nextCardLevel = card.levels.find((l) => !l.previousLevel);
      if (!nextCardLevel) {
        return responseParser({
          status: 422,
          request,
          body,
          message: t("Server.XNotFound", { x: t("CardLevel") }),
        });
      }
    } else {
      nextCardLevel = await prisma.cardLevel.findFirst({
        where: {
          cardId: Number(body.cardId),
          level: userCard.cardLevel.level + 1,
        },
      });
      if (!nextCardLevel) {
        return responseParser({
          status: 422,
          request,
          body,
          message: t("Server.XCantUpgradeCause", {
            x: t("Card"),
            cause: t("MaxLevel"),
          }),
        });
      }
    }

    const preWallet = await prisma.wallet.findUnique({
      where: { userId: telegramUser.id },
    });
    if (!preWallet) {
      return responseParser({
        status: 404,
        request,
        message: t("Server.XNotFound", { x: t("Wallet") }),
      });
    }
    const newBalance =
      walletParser(preWallet).balance -
      nextCardLevel.price +
      nextCardLevel.bonus;
    const newProfit = preWallet.profit + nextCardLevel.profit;
    if (newBalance < 0) {
      return responseParser({
        status: 422,
        request,
        body,
        message: t("Server.XNotEnough", {
          x: t("Balance"),
        }),
      });
    }
    const [newWallet, updatedUserCard] = await prisma.$transaction([
      prisma.wallet.update({
        where: { userId: telegramUser.id },
        data: {
          balance: newBalance,
          profit: newProfit,
          updatedAt: new Date(),
        },
      }),
      prisma.userCard.upsert({
        where: {
          userCardIdentifier: {
            userId: telegramUser.id,
            CardLevelId: userCard?.CardLevelId || 0,
          },
        },
        update: {
          CardLevelId: nextCardLevel.id,
        },
        include: {
          cardLevel: { include: { nextLevel: true } },
        },
        create: {
          CardLevelId: nextCardLevel.id,
          userId: telegramUser.id,
        },
      }),
    ]);
    return responseParser({
      data: {
        cardLevel: {
          id: updatedUserCard.cardLevel.id,
          cardId: updatedUserCard.cardLevel.cardId,
        },
        wallet: walletParser(newWallet),
      },
      status: 200,
      request,
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
