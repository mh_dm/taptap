import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  const t = await getLocale(request);
  const body: any = request.body;
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  try {
    const UserCards = await prisma.userCard.findMany({
      where: {
        userId: telegramUser.id,
      },
      include: {
        cardLevel: true,
      },
    });

    return responseParser({
      data: UserCards.map((level) => ({
        id: level.cardLevel.id,
        cardId: level.cardLevel.cardId,
      })),
      request,
    });
  } catch (e) {}
  return responseParser({ status: 500, request });
}
