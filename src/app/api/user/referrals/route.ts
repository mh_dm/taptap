import { friendParser } from "@/utils/api/firend";
import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  const t = await getLocale(request);
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  const user = await prisma.user.findUnique({
    where: { id: telegramUser.id },
    include: {
      friends: { include: { wallet: true } },
    },
  });

  if (user)
    return responseParser({
      data: user.friends.map(friendParser),
      request,
    });
  return responseParser({
    status: 404,
    message: t("Server.XNotFound", { x: t("User") }),
    request,
  });
}
