import { addUserIdToInitial, generateReferralCode } from "@/utils";
import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { userParser } from "@/utils/api/user";
import prisma from "@/utils/db";
import { cookies } from "next/headers";

export async function GET(request: Request) {
  const t = await getLocale(request);
  const cookieStore = cookies();
  const url = request.url;
  const { telegramUser, initData } = getTelegramUser(request);

  if (!telegramUser) {
    return responseParser({ status: 401, request });
  }

  try {
    const user = await prisma.user.upsert({
      where: { telegramId: telegramUser.telegramId },
      create: {
        identity: generateReferralCode(),
        telegramId: telegramUser.telegramId,
        username: telegramUser.username,
        firstName: telegramUser.firstName,
        lastName: telegramUser.lastName,
        avatar: telegramUser.photoUrl,
        invitedBy: telegramUser.referral
          ? { connect: { identity: telegramUser.referral } }
          : undefined,
        wallet: {
          create: {},
        },
        daily: {
          create: {},
        },
      },
      update: {
        username: telegramUser.username,
        firstName: telegramUser.firstName,
        lastName: telegramUser.lastName,
        avatar: telegramUser.photoUrl,
        wallet: {
          upsert: { create: {}, update: {} },
        },
        daily: {
          upsert: { create: {}, update: {} },
        },
      },
      include: {
        wallet: true,
        daily: true,
      },
    });
    cookieStore.set(
      "initData",
      addUserIdToInitial(initData, user.id, process.env.TELEGRAM_TOKEN)
    );
    return responseParser({
      data: userParser(user),
      request,
      message: t("Server.LoginSuccess"),
    });
  } catch (e: any) {
    if (e.code === "P2025") {
      try {
        const user = await prisma.user.create({
          data: {
            identity: generateReferralCode(),
            telegramId: telegramUser.telegramId,
            username: telegramUser.username,
            firstName: telegramUser.firstName,
            lastName: telegramUser.lastName,
            avatar: telegramUser.photoUrl,
            wallet: {
              create: {},
            },
            daily: {
              create: {},
            },
          },
          include: {
            wallet: true,
            daily: true,
          },
        });
        cookieStore.set(
          "initData",
          addUserIdToInitial(initData, user.id, process.env.TELEGRAM_TOKEN)
        );
        return responseParser({
          data: userParser(user),
          request,
          message: t("Server.SignUpSuccess"),
        });
      } catch (e) {
        return responseParser({ status: 500, request });
      }
    } else {
      return responseParser({ status: 500, request });
    }
  }
}
