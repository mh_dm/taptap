import { dailies } from "@/enums";
import { dailyParser, levelCalculator } from "@/utils/api/daily";
import { getLocale, getTelegramUser, responseParser } from "@/utils/api/parser";
import { walletParser } from "@/utils/api/wallet";
import prisma from "@/utils/db";
import dayjs from "dayjs";

export async function POST(request: Request) {
  const url = request.url;
  const t = await getLocale(request);
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });
  try {
    const daily = await prisma.daily.findUnique({
      where: { userId: telegramUser.id },
    });
    if (!daily)
      return responseParser({
        status: 404,
        request,
        message: t("Server.XNotFound", { x: t("Daily") }),
      });

    if (daily.updatedAt && dayjs().isBefore(dayjs(daily.updatedAt).endOf("D")))
      return responseParser({
        status: 451,
        request,
        message: t("Server.XNotTrustedCause", {
          x: t("Clime"),
          cause: t("TimeLimit"),
        }),
      });
    const newDailyLevel = daily.updatedAt
      ? dayjs().isBefore(dayjs(daily.updatedAt).add(1, "day").endOf("D"))
        ? daily.level + 1
        : 1
      : 1;
    const newDaily = await prisma.daily.update({
      where: { userId: telegramUser.id },
      data: {
        level: newDailyLevel,
        updatedAt: dayjs().toJSON(),
      },
    });

    const preWallet = await prisma.wallet.findUnique({
      where: { userId: telegramUser.id },
    });
    if (!preWallet) {
      return responseParser({
        status: 404,
        request,
        message: t("Server.XNotFound", { x: t("Wallet") }),
      });
    }
    const newBalance =
      walletParser(preWallet).balance +
      dailies[levelCalculator(newDailyLevel)].value;
    const newWallet = await prisma.wallet.update({
      where: { userId: telegramUser.id },
      data: {
        peakBalance: Math.max(newBalance, preWallet.peakBalance),
        balance: newBalance,
        updatedAt: new Date(),
      },
    });
    return responseParser({
      data: { daily: dailyParser(newDaily), wallet: walletParser(newWallet) },
      request,
      message: t("Server.DailyClaimedSuccess"),
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
