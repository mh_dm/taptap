import { dailyParser } from "@/utils/api/daily";
import { getTelegramUser, responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  const url = request.url;
  const { telegramUser } = getTelegramUser();
  if (!telegramUser) return responseParser({ status: 401, request });

  const daily = await prisma.daily.findUnique({
    where: { userId: telegramUser.id },
  });
  if (daily) return responseParser({ data: dailyParser(daily), request });
  return responseParser({ status: 500, request });
}
