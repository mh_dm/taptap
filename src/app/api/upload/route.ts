import { responseParser } from "@/utils/api/parser";
import fs from "fs";
import path from "path";
import { pipeline } from "stream";
import { promisify } from "util";
const pump = promisify(pipeline);

const fileLocation = "./public/storage/";
export async function POST(request: Request) {
  try {
    const formData = await request.formData();
    const file: any = formData.get("file");
    if (file.size > 1_000_000) {
      return responseParser({
        status: 406,
        request,
        message: "File Size must lower than 1MB",
      });
    }
    const dir = path.join(process.cwd(), fileLocation);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    const filePath = `${fileLocation}${new Date().getTime()}_${file?.name}`;
    await pump(file.stream(), fs.createWriteStream(filePath));
    return responseParser({
      status: 200,
      request,
      data: { url: filePath.slice(8) },
    });
  } catch (e) {
    return responseParser({ status: 500, request });
  }
}
