import { walletLevels } from "@/enums";
import { friendParser } from "@/utils/api/firend";
import { responseParser } from "@/utils/api/parser";
import prisma from "@/utils/db";

export async function GET(request: Request) {
  const url = request.url;
  const { search } = new URL(url);
  const params = new URLSearchParams(search);
  const level = params.get("level");
  if (!level || !(level in walletLevels)) {
    return responseParser({ status: 422, request });
  }

  const ranks = await prisma.user.findMany({
    take: 100,
    where: {
      wallet: {
        peakBalance: {
          gte: walletLevels[Number(level)].fromValue,
          lte: walletLevels[Number(level) + 1]?.fromValue,
        },
      },
    },
    include: {
      wallet: true,
    },
    orderBy: {
      wallet: {
        balance: "desc",
      },
    },
  });
  return responseParser({ data: ranks.map(friendParser), request });
}
