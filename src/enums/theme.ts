type LocaleType = {
  code: Locale;
  dir: Dir;
  name: string;
};

const locales: Record<Locale, LocaleType> = {
  en: {
    code: "en",
    dir: "ltr",
    name: "English",
  },
  fa: {
    code: "fa",
    name: "Persian",
    dir: "rtl",
  },
};

export { locales };
