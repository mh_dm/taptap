import {
  IconHexagon1,
  IconHexagon2,
  IconHexagon3,
  IconHexagon4,
  IconHexagon5,
  IconHexagon6,
  IconHexagon7,
  IconHexagon8,
} from "@tabler/icons-react";

interface TapStorage {
  level: number;
  price: number;
  value: number;
  isMax: boolean;
}

interface PerTap {
  level: number;
  price: number;
  value: number;
  isMax: boolean;
}

interface WalletLevel {
  level: number;
  fromValue: number;
  icon: TablerIcon;
  isMax: boolean;
}

const tapStorages: Record<number, TapStorage> = [...Array(20)]
  .map((a, i) => i + 1)
  .reduce((pre, cur) => {
    return {
      ...pre,
      [cur]: {
        level: cur,
        price: 2 ** (cur + 9),
        isMax: cur === 20,
        value: 1000 + (cur - 1) * 500,
      },
    };
  }, {});

const perTaps: Record<number, PerTap> = [...Array(20)]
  .map((a, i) => i + 1)
  .reduce((pre, cur) => {
    return {
      ...pre,
      [cur]: {
        level: cur,
        price: 2 ** (cur + 9),
        isMax: cur === 20,
        value: cur,
      },
    };
  }, {});

const walletLevels: Record<number, WalletLevel> = {
  1: { level: 1, fromValue: 0, isMax: false, icon: IconHexagon1 },
  2: { level: 2, fromValue: 10 ** 2, isMax: false, icon: IconHexagon2 },
  3: { level: 3, fromValue: 10 ** 3, isMax: false, icon: IconHexagon3 },
  4: { level: 4, fromValue: 10 ** 4, isMax: false, icon: IconHexagon4 },
  5: { level: 5, fromValue: 10 ** 5, isMax: false, icon: IconHexagon5 },
  6: { level: 6, fromValue: 10 ** 6, isMax: false, icon: IconHexagon6 },
  7: { level: 7, fromValue: 10 ** 7, isMax: false, icon: IconHexagon7 },
  8: { level: 8, fromValue: 10 ** 8, isMax: true, icon: IconHexagon8 },
};
export { tapStorages, perTaps, walletLevels };
