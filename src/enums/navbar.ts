import {
  IconChecklist,
  IconFriends,
  IconHome,
  IconMedal2,
} from "@tabler/icons-react";

type NavbarItemType =
  | {
      name: string;
      url: string;
      disabled?: boolean;
    } & (
      | {
          icon: TablerIcon;
        }
      | {
          image: string;
        }
    );

const navbarItems: NavbarItemType[] = [
  { icon: IconHome, name: "Home", url: "/", disabled: false },
  { icon: IconChecklist, name: "Tasks", url: "/tasks", disabled: false },
  { icon: IconFriends, name: "Friends", url: "/friends", disabled: false },
  { icon: IconMedal2, name: "GiftCenter", url: "/gift-center", disabled: false },
  // { icon: "/next.svg", name: "airdrop", url: "/" },
];

export { navbarItems };
export type { NavbarItemType };
