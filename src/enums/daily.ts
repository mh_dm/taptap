import {
  IconHexagon1,
  IconHexagon2,
  IconHexagon3,
  IconHexagon4,
  IconHexagon5,
  IconHexagon6,
  IconHexagon7,
} from "@tabler/icons-react";

interface Daily {
  level: number;
  value: number;
  isMax: boolean;
  icon: TablerIcon;
}

const dailies: Record<number, Daily> = {
  1: { level: 1, value: 2 ** 15, isMax: false, icon: IconHexagon1 },
  2: { level: 2, value: 2 ** 16, isMax: false, icon: IconHexagon2 },
  3: { level: 3, value: 2 ** 17, isMax: false, icon: IconHexagon3 },
  4: { level: 4, value: 2 ** 18, isMax: false, icon: IconHexagon4 },
  5: { level: 5, value: 2 ** 19, isMax: false, icon: IconHexagon5 },
  6: { level: 6, value: 2 ** 20, isMax: false, icon: IconHexagon6 },
  7: { level: 7, value: 2 ** 21, isMax: true, icon: IconHexagon7 },
};

export { dailies };
