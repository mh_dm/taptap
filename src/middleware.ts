import createMiddleware from "next-intl/middleware";
import { locales } from "./enums";

export default createMiddleware({
  locales: Object.keys(locales),
  defaultLocale: "en",
});

export const config = {
  matcher: ["/((?!api|_next|.*\\..*).*)"],
};
