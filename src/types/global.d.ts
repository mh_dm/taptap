import { languages } from "@/enums";
import { Icon, IconProps } from "@tabler/icons-react";
declare global {
  type Dir = "ltr" | "rtl";
  type Locale = "en" | "fa";
  type TablerIcon = React.ForwardRefExoticComponent<
    IconProps & React.RefAttributes<Icon>
  >;
}
