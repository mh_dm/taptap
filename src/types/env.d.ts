declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: "development" | "production" | "test";
    TELEGRAM_TOKEN: string;
    NEXT_PUBLIC_BOT_LINK: string;
    NEXT_PUBLIC_INIT_DATA: string;
    NEXT_PUBLIC_NODE: "development" | "production";
    URL: string;
  }
}
