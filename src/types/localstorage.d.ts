export interface TapStorage {
  taps: number;
  startTime: number;
  endTime: number;
}
