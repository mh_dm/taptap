import { Card, CardLevel } from "@prisma/client";

interface CardLevelType extends Omit<CardLevel, "createdAt" | "nextLevelId"> {
  isMaxLevel: boolean;
  profitDelta: number;
}

interface CardType extends Card, CardLevelType {}
interface CardLevelPro extends CardLevel {
  nextLevel?: CardLevel | null;
}

interface CardLevelMinimal extends Pick<CardLevel, "id" | "cardId"> {}
