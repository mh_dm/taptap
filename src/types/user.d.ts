interface Wallet {
  id: number;
  userId: number;
  profit: number;
  balance: number;
  peakBalance: number;
  level: number;
  tap: number;
  charge: number;
  storage: number;
  updatedAt: Date | null;
}
interface User {
  id: number;
  identity: string;
  username: string;
  firstName: string;
  lastName: string;
  avatar: string;
  mobile: string;
  wallet: Wallet;
  createdAt: Date;
}

interface Daily {
  id: number;
  userId: number;
  level: number;
  updatedAt: Date | null;
  activeAt: Date | null;
}

type InviteUser = Pick<
  User,
  "identity" | "firstName" | "lastName" | "avatar"
> & { wallet: Pick<Wallet, "balance" | "profit"> };
[];
