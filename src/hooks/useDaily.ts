/* eslint-disable react-hooks/exhaustive-deps */
import api from "@/services";
import { levelCalculator } from "@/utils/api/daily";
import dayjs from "dayjs";
import { useCallback } from "react";
import { create } from "zustand";
import { useWallet } from "./useWallet";

interface DailyStorage {
  daily: Daily;
  activeLevel: number;
  setDaily: (daily: Daily) => void;
  loading: boolean;
}

interface UseDaily
  extends Pick<DailyStorage, "daily" | "loading" | "setDaily" | "activeLevel"> {
  onClime: () => void;
}

const dailyStorage = create<DailyStorage>((set, get) => ({
  daily: {
    id: 0,
    level: 0,
    userId: 0,
    activeAt: null,
    updatedAt: null,
  },
  activeLevel: 1,
  loading: true,
  setDaily: (daily) =>
    set((state) => {
      return {
        ...state,
        daily: {
          ...daily,
        },
        activeLevel: dayjs().isAfter(daily.activeAt)
          ? levelCalculator(daily.level + 1)
          : 0,
        loading: false,
      };
    }),
}));

export const useDaily = (): UseDaily => {
  const {
    daily,
    loading,
    setDaily: setDailyStorage,
    activeLevel,
  } = dailyStorage();
  const { setWallet } = useWallet();
  const onClime = useCallback((): Promise<Daily> => {
    return new Promise((resolve, reject) => {
      api
        .post("/api/user/daily/claim")
        .then((daily) => {
          setDailyStorage(daily.data.daily);
          setWallet(daily.data.wallet);
          resolve(daily.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }, []);

  const setDaily = useCallback((daily: Daily) => {
    setDailyStorage(daily);
  }, []);

  return {
    activeLevel,
    loading,
    onClime,
    setDaily,
    daily,
  };
};
