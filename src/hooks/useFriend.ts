import api from "@/services";
import { useCallback, useEffect, useRef } from "react";
import { create } from "zustand";

interface FriendStorage {
  friends: InviteUser[];
  setFriend: (user: InviteUser[]) => void;
  loading: boolean;
  remainingSec: number;
  setRemainingSec: (sec: number) => void;
  setLoading: () => void;
  lastCall: Date | null;
}

interface UseFriends
  extends Pick<
    FriendStorage,
    "friends" | "loading" | "lastCall" | "remainingSec"
  > {
  reload: () => void;
}

const friendsStorage = create<FriendStorage>((set, get) => ({
  friends: [],
  loading: true,
  lastCall: null,
  remainingSec: 0,
  setLoading: () =>
    set((state) => ({
      ...state,
      loading: true,
    })),
  setRemainingSec: (sec) =>
    set((state) => ({
      ...state,
      remainingSec: sec,
    })),
  setFriend: (friends) =>
    set((state) => ({
      ...state,
      friends,
      loading: false,
      lastCall: new Date(),
    })),
}));

let called = false;
export const useFriends = (): UseFriends => {
  const {
    setRemainingSec,
    remainingSec,
    friends,
    setFriend,
    loading,
    lastCall,
    setLoading,
  } = friendsStorage();
  const intervalRef = useRef<{ id: NodeJS.Timeout | null; sec: number }>({
    id: null,
    sec: 0,
  });
  const getUser = useCallback(() => {
    if (
      !called ||
      (lastCall && new Date().getTime() - lastCall.getTime() > 60000)
    ) {
      called = true;
      setLoading();
      if (intervalRef.current.id) clearInterval(intervalRef.current.id);
      intervalRef.current.sec = 60;
      intervalRef.current.id = setInterval(() => {
        setRemainingSec(intervalRef.current.sec--);
        if (intervalRef.current.sec === -1) {
          intervalRef.current.id && clearInterval(intervalRef.current.id);
        }
      }, 1000);
      api.get("/api/user/referrals").then((res) => {
        setFriend(res.data);
      });
    }
  }, [lastCall]);

  useEffect(() => {
    getUser();
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    friends,
    loading,
    remainingSec,
    lastCall,
    reload: getUser,
  };
};
