import { User } from "@prisma/client";
import { useEffect, useRef } from "react";
import { create } from "zustand";
import { useWallet } from "./useWallet";
import api from "@/services";
import { useDaily } from "./useDaily";
import { useRouter } from "@/utils/navigation";

interface UserStorage {
  user: User;
  setUser: (user: User) => void;
  loading: boolean;
}

interface UseUser extends Pick<UserStorage, "user" | "loading"> {}

const cartStorage = create<UserStorage>((set, get) => ({
  user: {
    id: 0,
    telegramId: "",
    identity: "",
    mobile: null,
    avatar: null,
    firstName: null,
    lastName: null,
    createdAt: new Date(),
    username: null,
    invitedByIdentity: null,
  },
  loading: true,
  setUser: (user) => set((state) => ({ ...state, user, loading: false })),
}));
let called = false;
export const useUser = (): UseUser => {
  const { user, setUser, loading } = cartStorage();
  const { setWallet } = useWallet();
  const { setDaily } = useDaily();
  const intervalRef = useRef<NodeJS.Timeout | undefined>();
  const router = useRouter();
  useEffect(() => {
    if (called) return;
    intervalRef.current = setInterval(() => {
      const initData =
        process.env.NEXT_PUBLIC_NODE === "production"
          ? (window as any)?.Telegram?.WebApp?.initData
          : "user=%7B%22id%22%3A107546604%2C%22first_name%22%3A%22%F0%9D%99%BC%F0%9D%99%BE%F0%9D%99%B7%F0%9D%99%B0%F0%9D%99%BC%F0%9D%99%BC%F0%9D%99%B0%F0%9D%99%B3%20%F0%9D%99%B7%22%2C%22last_name%22%3A%22%22%2C%22username%22%3A%22MH_DM%22%2C%22language_code%22%3A%22en%22%2C%22allows_write_to_pm%22%3Atrue%7D&chat_instance=-8454637327437198090&chat_type=group&auth_date=1718645773&hash=b39ab25b40113600fef62c480f4e8f3a23b05472ff9d680f7d5e4137276c3133";
      if (initData) {
        (window as any)?.Telegram?.WebApp?.expand?.();
        (window as any)?.Telegram?.WebApp?.setHeaderColor?.("#0f172a");
        // const languageCode = (window as any)?.Telegram?.WebApp?.initDataUnsafe
        //   ?.user?.language_code;
        // if (languageCode) {
        //   router.replace("/", { locale: languageCode });
        // }

        clearInterval(intervalRef.current);

        !called &&
          api.get(`/api/user?${initData}`).then((res) => {
            const { wallet, daily, ...user } = res.data;
            setWallet(wallet);
            setDaily(daily);
            setUser(user);
          });
      }
      called = true;
    }, 500);
    return () => {
      intervalRef.current && clearInterval(intervalRef.current);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    loading,
    user,
  };
};
