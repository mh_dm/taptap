/* eslint-disable react-hooks/exhaustive-deps */
import { perTaps, tapStorages } from "@/enums";
import api from "@/services";
import { TapStorage } from "@/types/localstorage";
import { debounce, getStorageTaps, setStorageTaps } from "@/utils";
import { useCallback, useEffect, useRef } from "react";
import { create } from "zustand";

interface WalletStorage {
  wallet: Wallet;
  setWallet: (wallet: Wallet) => void;
  onTap: () => void;
  onProfitEffect: () => void;
  charge: () => void;
  loading: boolean;
  called: boolean;
  call: () => void;
}

interface UseUser
  extends Pick<WalletStorage, "wallet" | "loading" | "setWallet" | "onTap"> {
  upgradeTap: () => Promise<Wallet>;
  upgradeStorage: () => Promise<Wallet>;
}

const cartStorage = create<WalletStorage>((set, get) => ({
  wallet: {
    id: 0,
    level: 1,
    peakBalance: 0,
    userId: 0,
    profit: 0,
    balance: 0,
    tap: 1,
    charge: 0,
    storage: 1,
    updatedAt: new Date(),
  },
  loading: true,
  called: false,
  setWallet: (wallet) =>
    set((state) => {
      const charge = Math.min(
        wallet.charge +
          parseInt(
            ((new Date().getTime() -
              (new Date(wallet.updatedAt || new Date()).getTime() || 0)) *
              3) /
              1000 +
              ""
          ),
        tapStorages[wallet.storage].value
      );
      return {
        ...state,
        wallet: {
          ...wallet,
          charge,
        },
        loading: false,
      };
    }),
  onTap: () =>
    set((state) => {
      if (state.wallet.charge - perTaps[state.wallet.tap].value >= 0) {
        return {
          ...state,
          wallet: {
            ...state.wallet,
            balance: state.wallet.balance + perTaps[state.wallet.tap].value,
            charge: state.wallet.charge - perTaps[state.wallet.tap].value,
          },
        };
      }
      return state;
    }),
  onProfitEffect: () =>
    set((state) => {
      return {
        ...state,
        wallet: {
          ...state.wallet,
          balance: state.wallet.balance + state.wallet.profit / 3600,
        },
      };
    }),
  call: () => set((state) => ({ ...state, called: true })),
  charge: () =>
    set((state) => {
      if (state.wallet.charge === tapStorages[state.wallet.storage].value) {
        return state;
      } else if (
        state.wallet.charge + 3 >=
        tapStorages[state.wallet.storage].value
      ) {
        return {
          ...state,
          wallet: {
            ...state.wallet,
            charge: tapStorages[state.wallet.storage].value,
          },
        };
      } else {
        return {
          ...state,
          wallet: { ...state.wallet, charge: state.wallet.charge + 3 },
        };
      }
    }),
}));

let intervalRef: NodeJS.Timeout | null = null;
export const useWallet = (): UseUser => {
  const {
    wallet,
    setWallet: setW,
    loading,
    onTap: onTapFunction,
    charge,
    onProfitEffect,
  } = cartStorage();
  const tapsRef = useRef<TapStorage>(
    getStorageTaps() || { taps: 0, startTime: 0, endTime: 0 }
  );
  useEffect(() => {
    if (intervalRef) return;
    if (tapsRef.current.taps > 0) {
      postTaps();
    }
    intervalRef = setInterval(() => {
      charge();
      onProfitEffect();
    }, 1000);
  }, []);

  const postTaps = useCallback(() => {
    api
      .post("/api/user/wallet/tap", tapsRef.current)
      .then((res) => {
        setW(res.data);
      })
      .catch((e) => {
        e.data && setW(e.data);
      });
    tapsRef.current.taps = 0;
    setStorageTaps(tapsRef.current);
  }, []);
  const onTapDebounce = debounce(() => {
    postTaps();
  }, 5000);
  const onTap = useCallback(() => {
    if (tapsRef.current.taps === 0) {
      tapsRef.current.startTime = new Date().getTime();
    }
    tapsRef.current.endTime = new Date().getTime();
    tapsRef.current.taps++;
    setStorageTaps(tapsRef.current);
    onTapDebounce();
    onTapFunction();
  }, []);

  const upgradeTap = useCallback((): Promise<Wallet> => {
    return new Promise((resolve, reject) => {
      api
        .post("/api/user/wallet/up_tap")
        .then((res) => {
          setW(res.data);
          resolve(res.data);
        })
        .catch(() => {
          reject();
        });
    });
  }, []);
  const upgradeStorage = useCallback((): Promise<Wallet> => {
    return new Promise((resolve, reject) => {
      api
        .post("/api/user/wallet/up_storage")
        .then((res) => {
          setW(res.data);
          resolve(res.data);
        })
        .catch(() => {
          reject();
        });
    });
  }, []);
  const setWallet = useCallback((wallet: Wallet): void => {
    setW(wallet);
  }, []);
  return {
    loading,
    wallet,
    setWallet,
    upgradeTap,
    upgradeStorage,
    onTap,
  };
};
