import { isClientSide } from "@/utils";
import { ParserResponseType } from "@/utils/api/parser";
import toast from "react-hot-toast";

type ToastLevel = "all" | "error" | "no";
function fetcher(
  url: string,
  options: any = {},
  toastLevel: ToastLevel = "all"
) {
  return new Promise<ParserResponseType>((resolve, reject) => {
    let toastId: string;
    if (isClientSide() && toastLevel === "all") {
      toastId = toast.loading("loading");
    }
    fetch(url, {
      method: "GET",
      ...options,
    })
      .then((response) => {
        if (response.ok)
          response.json().then((a) => {
            if (isClientSide() && toastLevel === "all") {
              toast.success(a.message, {
                id: toastId,
              });
            }
            resolve(a);
          });
        else
          response.json().then((a) => {
            if (isClientSide() && toastLevel === "error") {
              toast.error(a.message);
            } else if (isClientSide() && toastLevel === "all") {
              toast.error(a.message, {
                id: toastId,
              });
            }
            reject(a);
          });
      })
      .catch((e) => {
        console.log(`[service: ${e.message}]`);
      });
  });
}
function post(
  url: string,
  data: any = {},
  options: RequestInit = {},
  haveToast?: ToastLevel
) {
  return fetcher(
    url,
    {
      method: "POST",
      body: JSON.stringify(
        Object.keys(data).reduce((pre: any, key: any) => {
          return {
            ...pre,
            [key]: data[key] || undefined,
          };
        }, {})
      ),
      ...options,
      headers: {
        "Content-Type": "application/json",
        ...(options.headers || {}),
      },
    },
    haveToast
  );
}

function upload(
  url: string,
  formData: any,
  options: RequestInit = {},
  haveToast?: ToastLevel
) {
  return fetcher(
    url,
    {
      method: "POST",
      body: formData,
      ...options,
    },
    haveToast
  );
}

function get(
  url: string,
  data: object = {},
  options: RequestInit = {},
  haveToast?: ToastLevel
) {
  let str = "";
  Object.entries(JSON.parse(JSON.stringify(data))).forEach((d, i) => {
    str += (i ? "&" : "?") + d[0] + "=" + d[1];
  });
  return fetcher(
    url + str,
    {
      ...options,
      headers: {
        "Content-Type": "application/json",
        ...(options.headers || {}),
      },
    },
    haveToast
  );
}

const api = {
  post,
  get,
  upload
};

export default api;
