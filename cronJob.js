const cron = require('node-cron');
const { exec } = require('child_process');

cron.schedule(' */15 * * * *', () => {
  console.log('Running monitoring script...');
  exec('node src/utils/monitor/monitor.js', (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing script: ${error.message}`);
      return;
    }

    if (stderr) {
      console.error(`Script stderr: ${stderr}`);
      return;
    }

    console.log(`Script output: ${stdout}`);
  });
});