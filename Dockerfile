FROM node:20.14.0

WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm i
COPY . ./
RUN chmod +x entrypoint.sh
RUN npx prisma generate
RUN npm run build
