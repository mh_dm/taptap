-- CreateTable
CREATE TABLE "Monitor" (
    "id" SERIAL NOT NULL,
    "balanceSum" INTEGER NOT NULL,
    "userCount" INTEGER NOT NULL,
    "completedDaily" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Monitor_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Monitor_id_key" ON "Monitor"("id");
