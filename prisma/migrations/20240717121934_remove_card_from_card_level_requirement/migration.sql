/*
  Warnings:

  - You are about to drop the `CardRequirement` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "CardRequirement" DROP CONSTRAINT "CardRequirement_cardId_fkey";

-- DropForeignKey
ALTER TABLE "CardRequirement" DROP CONSTRAINT "CardRequirement_prerequisiteCardLevelId_fkey";

-- DropTable
DROP TABLE "CardRequirement";

-- CreateTable
CREATE TABLE "CardLevelRequirement" (
    "id" SERIAL NOT NULL,
    "prerequisiteCardLevelId" INTEGER,
    "friends" INTEGER NOT NULL DEFAULT 0,
    "extraFriends" INTEGER NOT NULL DEFAULT 0,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "CardLevelRequirement_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "CardLevelRequirement" ADD CONSTRAINT "CardLevelRequirement_prerequisiteCardLevelId_fkey" FOREIGN KEY ("prerequisiteCardLevelId") REFERENCES "CardLevel"("id") ON DELETE SET NULL ON UPDATE CASCADE;
