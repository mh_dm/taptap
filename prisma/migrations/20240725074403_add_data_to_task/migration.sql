/*
  Warnings:

  - You are about to drop the column `profit` on the `Task` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Task" DROP COLUMN "profit",
ADD COLUMN     "data" JSONB NOT NULL DEFAULT '{}',
ALTER COLUMN "description" DROP NOT NULL;
