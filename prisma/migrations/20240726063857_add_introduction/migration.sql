-- CreateTable
CREATE TABLE "Introduction" (
    "id" SERIAL NOT NULL,
    "rank" INTEGER NOT NULL,
    "image" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,

    CONSTRAINT "Introduction_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Introduction_id_key" ON "Introduction"("id");
