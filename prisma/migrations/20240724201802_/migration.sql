/*
  Warnings:

  - A unique constraint covering the columns `[CardLevelId,userId]` on the table `UserCard` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "UserCard_userId_key";

-- CreateIndex
CREATE UNIQUE INDEX "UserCard_CardLevelId_userId_key" ON "UserCard"("CardLevelId", "userId");
