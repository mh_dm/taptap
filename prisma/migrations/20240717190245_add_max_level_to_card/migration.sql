/*
  Warnings:

  - A unique constraint covering the columns `[id]` on the table `CardLevel` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "Card" ADD COLUMN     "maxLevel" INTEGER NOT NULL DEFAULT 1;

-- AlterTable
ALTER TABLE "CardLevel" ALTER COLUMN "price" SET DEFAULT 100,
ALTER COLUMN "level" SET DEFAULT 1,
ALTER COLUMN "balance" SET DEFAULT 0,
ALTER COLUMN "profit" SET DEFAULT 1;

-- CreateIndex
CREATE UNIQUE INDEX "CardLevel_id_key" ON "CardLevel"("id");
