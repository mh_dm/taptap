/*
  Warnings:

  - You are about to drop the column `balancePick` on the `Wallet` table. All the data in the column will be lost.
  - You are about to drop the column `level` on the `Wallet` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Wallet" DROP COLUMN "balancePick",
DROP COLUMN "level",
ADD COLUMN     "peakBalance" INTEGER NOT NULL DEFAULT 0;
