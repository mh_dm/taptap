/*
  Warnings:

  - You are about to drop the column `comletedAt` on the `UserTask` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "UserTask" DROP COLUMN "comletedAt",
ADD COLUMN     "completedAt" TIMESTAMP(3);
