-- CreateTable
CREATE TABLE "UserCard" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER NOT NULL,
    "CardLevelId" INTEGER NOT NULL,

    CONSTRAINT "UserCard_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Card" (
    "id" SERIAL NOT NULL,
    "image" BYTEA NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "upgradeTitle" TEXT NOT NULL,
    "upgradeDescription" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Card_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CardRequirement" (
    "id" SERIAL NOT NULL,
    "cardId" INTEGER NOT NULL,
    "prerequisiteCardLevelId" INTEGER,
    "friends" INTEGER NOT NULL DEFAULT 0,
    "extraFriends" INTEGER NOT NULL DEFAULT 0,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "CardRequirement_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CardLevel" (
    "id" SERIAL NOT NULL,
    "cardId" INTEGER NOT NULL,
    "price" INTEGER NOT NULL,
    "level" INTEGER NOT NULL,
    "balance" INTEGER NOT NULL,
    "profit" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "CardLevel_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "UserCard_userId_key" ON "UserCard"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "UserCard_CardLevelId_key" ON "UserCard"("CardLevelId");

-- CreateIndex
CREATE UNIQUE INDEX "CardRequirement_cardId_key" ON "CardRequirement"("cardId");

-- CreateIndex
CREATE UNIQUE INDEX "CardLevel_cardId_level_key" ON "CardLevel"("cardId", "level");

-- AddForeignKey
ALTER TABLE "UserCard" ADD CONSTRAINT "UserCard_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserCard" ADD CONSTRAINT "UserCard_CardLevelId_fkey" FOREIGN KEY ("CardLevelId") REFERENCES "CardLevel"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CardRequirement" ADD CONSTRAINT "CardRequirement_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CardRequirement" ADD CONSTRAINT "CardRequirement_prerequisiteCardLevelId_fkey" FOREIGN KEY ("prerequisiteCardLevelId") REFERENCES "CardLevel"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CardLevel" ADD CONSTRAINT "CardLevel_cardId_fkey" FOREIGN KEY ("cardId") REFERENCES "Card"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
