/*
  Warnings:

  - You are about to drop the column `maxLevel` on the `Card` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[id]` on the table `Card` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[nextLevelId]` on the table `CardLevel` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `CardLevelRequirement` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[mobile]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `UserCard` will be added. If there are existing duplicate values, this will fail.
  - Made the column `description` on table `Card` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Card" DROP COLUMN "maxLevel",
ALTER COLUMN "description" SET NOT NULL,
ALTER COLUMN "upgradeTitle" DROP NOT NULL,
ALTER COLUMN "upgradeDescription" DROP NOT NULL;

-- AlterTable
ALTER TABLE "CardLevel" ADD COLUMN     "nextLevelId" INTEGER;

-- CreateIndex
CREATE UNIQUE INDEX "Card_id_key" ON "Card"("id");

-- CreateIndex
CREATE UNIQUE INDEX "CardLevel_nextLevelId_key" ON "CardLevel"("nextLevelId");

-- CreateIndex
CREATE UNIQUE INDEX "CardLevelRequirement_id_key" ON "CardLevelRequirement"("id");

-- CreateIndex
CREATE UNIQUE INDEX "User_mobile_key" ON "User"("mobile");

-- CreateIndex
CREATE UNIQUE INDEX "UserCard_id_key" ON "UserCard"("id");

-- AddForeignKey
ALTER TABLE "CardLevel" ADD CONSTRAINT "CardLevel_nextLevelId_fkey" FOREIGN KEY ("nextLevelId") REFERENCES "CardLevel"("id") ON DELETE SET NULL ON UPDATE CASCADE;
