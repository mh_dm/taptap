/*
  Warnings:

  - You are about to drop the column `balance` on the `CardLevel` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Card" ADD COLUMN     "expiredAt" TIMESTAMP(3);

-- AlterTable
ALTER TABLE "CardLevel" DROP COLUMN "balance",
ADD COLUMN     "bonus" INTEGER NOT NULL DEFAULT 0,
ALTER COLUMN "profit" SET DEFAULT 0;

-- AlterTable
ALTER TABLE "UserCard" ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;
