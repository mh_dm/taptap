import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["var(--local-fonts)"],
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      boxShadow: {
        "inner-2": "inset 0 2px 4px 0 rgb(0 0 0 / 0.3)",
      },
      keyframes: {
        bubbleEffect: {
          "0%": { transform: "scale(1)", opacity: "1" },
          "100%": {
            transform: "scale(1.2) translateY(-12.5rem)",
            opacity: "0",
          },
        },
        "slide-up": {
          "0%": { transform: "translateY(100%)" },
          "100%": { transform: "translateY(0)" },
        },
        "slide-down": {
          "0%": { transform: "translateY(0)" },
          "100%": { transform: "translateY(100%)" },
        },
      },

      animation: {
        bubble: "bubbleEffect 1000ms ease-out forwards",
        "slide-up": "slide-up 0.2s ease-in-out",
        "slide-down": "slide-down 0.2s ease-in-out",
      },
    },
  },
  plugins: [],
};
export default config;
